<?php declare(strict_types=1);

use M360\Registry;
use PHPUnit\Framework\TestCase;

/**
 * @covers M360\Registry
 * @covers M360\CustomRegistry
 * @covers M360\Resource
 * @covers M360\Databases
 */
final class _08RegistryTest extends TestCase
{
	public function testRegistrySetGetGtwConnector (): void {

		$registry = new Registry();
		$gwConnect = new \M360\gateway\Connector("myService", 1);
		$registry->setGatewayConnector($gwConnect);
		$this->assertNotEmpty($registry);
		$connector = $registry->getGatewayConnector();
		$this->assertNotEmpty($connector);
	}

	public function testRegistryLoad(): void {
		$_SERVER["MW_TESTING"] = 1;
		$registry = new Registry();

		$stub1 = $this->createStub(\M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn((object)[
			'registry' => (object)[
				'_TTL' => 2000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		]);
		$registry->setGatewayConnector($stub1);
		$response = $registry->load();
		$this->assertEquals($response, true);
	}

	public function testRegistryGetUsingCache(): void {
		$_SERVER["MW_TESTING"] = 1;
		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 2000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];

		$stub2 = $this->createStub(M360\M360Cache::class);
		$stub2->method('get')->willReturn($gtwRegistry->registry);
		$registry = new Registry($stub2);

		$stub1 = $this->createStub(\M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn($gtwRegistry);
		$registry->setGatewayConnector($stub1);
		$response = $registry->get('registry', 'logger');
		$this->assertNotEmpty($response);
	}

	public function testRegistryGetNoCache(): void {
		$_SERVER["MW_TESTING"] = 1;
		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 2000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];

		$stub2 = $this->createMock(M360\M360Cache::class);
		$stub2->method('get')->willReturnOnConsecutiveCalls(null, $gtwRegistry->registry);
		$registry = new Registry($stub2);

		$stub1 = $this->createStub(\M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn($gtwRegistry);
		$registry->setGatewayConnector($stub1);

		$response = $registry->get('registry', 'logger');
		$this->assertNotEmpty($response);
	}

	public function testRegistryGetNoCacheFull(): void {
		$_SERVER["MW_TESTING"] = 1;
		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 2000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];

		$stub2 = $this->createMock(M360\M360Cache::class);
		$stub2->method('get')->willReturnOnConsecutiveCalls(null, $gtwRegistry->registry);
		$registry = new Registry($stub2);

		$stub1 = $this->createStub(\M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn($gtwRegistry);
		$registry->setGatewayConnector($stub1);

		$response = $registry->get('registry');
		$this->assertNotEmpty($response);
	}

	public function testRegistryGetNoCacheButNull(): void {
		$_SERVER["MW_TESTING"] = 1;
		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 2000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];

		$stub2 = $this->createMock(M360\M360Cache::class);
		$stub2->method('get')->willReturn(null);
		$registry = new Registry($stub2);

		$stub1 = $this->createStub(\M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn($gtwRegistry);
		$registry->setGatewayConnector($stub1);

		$response = $registry->get('registry', 'logger');
		$this->assertEquals($response, false);
	}

	public function testReloadAndClear(): void {
		$_SERVER["MW_TESTING"] = 1;
		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 2000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];

		$registry = new Registry();
		$stub1 = $this->createStub(\M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn($gtwRegistry);
		$registry->setGatewayConnector($stub1);

		$response = $registry->reload();
		$this->assertNotEmpty($response);

		$response = $registry->clear();
		$this->assertTrue($response);
	}

	public function testGetCustomAndResourceOk(): void {
		$_SERVER["MW_TESTING"] = 1;
		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];

		$stub2 = $this->createMock(M360\M360Cache::class);
		$stub2->method('get')->willReturnOnConsecutiveCalls($gtwRegistry->registry, $gtwRegistry->custom, $gtwRegistry->resources);
		$registry = new Registry($stub2);

		$stub1 = $this->createStub(\M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn($gtwRegistry);
		$registry->setGatewayConnector($stub1);

		$response = $registry->load();
		$this->assertTrue($response);
		$response = $registry->get('registry');
		$this->assertNotEmpty($response);

		$customRegistry = new \M360\CustomRegistry($stub2);
		$response = $customRegistry->get('custom', 'mail_sendgrid');
		$this->assertNotEmpty($response);

		$resource = new \M360\Resource($stub2);
		$response = $resource->get('resources', 'sql_resource');
		$this->assertNotEmpty($response);
	}

	public function testGetDatabaseOk(): void {
		$_SERVER["MW_TESTING"] = 1;
		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];

		$stub2 = $this->createMock(M360\M360Cache::class);
		$stub2->method('get')->willReturn($gtwRegistry->databases);
		$registry = new Registry($stub2);

		$stub1 = $this->createStub(\M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn($gtwRegistry);
		$registry->setGatewayConnector($stub1);

		$response = $registry->load();
		$this->assertTrue($response);
		$response = $registry->get('registry');
		$this->assertNotEmpty($response);

		$customRegistry = new \M360\Databases($stub2);
		$response = $customRegistry->getDb('single', 'example2');
		$this->assertNotEmpty($response);

		$response = $customRegistry->getDb('single', 'example3');
		$this->assertNotEmpty($response);

		$response = $customRegistry->getDb('multitenant', 'example1', 'MIKE');
		$this->assertNotEmpty($response);

		$response = $customRegistry->getDb('multitenant', 'example4', 'MIKE');
		$this->assertNotEmpty($response);

		$response = $customRegistry->getDb('multitenant', '', 'MIKE');
		$this->assertNotEmpty($response);

		$response = $customRegistry->getDb('multitenant', 'example1');
		$this->assertEmpty($response);

	}

	public function testGetDatabaseReturnsNull(): void {
		$_SERVER["MW_TESTING"] = 1;
		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];

		$stub2 = $this->createMock(M360\M360Cache::class);
		$stub2->method('get')->willReturnOnConsecutiveCalls($gtwRegistry->registry, null);
		$registry = new Registry($stub2);

		$stub1 = $this->createStub(\M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn($gtwRegistry);
		$registry->setGatewayConnector($stub1);

		$response = $registry->load();
		$this->assertTrue($response);
		$response = $registry->get('registry');
		$this->assertNotEmpty($response);

		$customRegistry = new \M360\Databases($stub2);
		$response = $customRegistry->getDb('single', 'example2');
		$this->assertEmpty($response);

	}
}