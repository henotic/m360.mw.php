<?php declare(strict_types=1);

use M360\Service;
use PHPUnit\Framework\TestCase;

/**
 * @covers M360\Service
 * @covers M360\timeConvert\Time
 */
final class _02ServiceTest extends TestCase
{
	public function testInitInvalidConfiguration1 (): void {
		$service = new Service();
		$response = $service->init([
			'platform' => 'manual',
			'contract' => 'invalid',
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		]);
		$this->assertEmpty($response);
	}

	public function testInitInvalidConfiguration2 (): void {

		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();

		$service = new Service();
		$response = $service->init([
			'platform' => 'manual',
			'contract' => 'invalid',
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		], $stub1);
		$this->assertEmpty($response);
	}

	public function testEnsureIPAddressKubernetes (): void {

		$service = new Service();
		$response = $service->init((object)[
			'platform' => 'kubernetes',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"exposedPort" => 80
			]
		]);

		$service->ensureIPAddress();

		$this->assertEmpty($response);
	}

	public function testEnsureIPAddressManual (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];
		$service = new Service();
		$response = $service->init($configuration);
		$mockedDNSGETRECORD = function(){
			return [
				[
					'class'=> 'IN',
					'type'=> 'A',
					'ip'=> '127.0.0.1',
				]
			];
		};
		$service->ensureIPAddress($mockedDNSGETRECORD);
		$this->assertEquals($configuration->ip, '127.0.0.1');
	}

	public function testEnsureIPAddressFail (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];
		$service = new Service();
		$response = $service->init($configuration);
		$mockedDNSGETRECORD = function(){
			throw new Exception('Dummy Error!');
		};
		$service->ensureIPAddress($mockedDNSGETRECORD);
		$this->assertEquals($configuration->ip, '127.0.0.1');
	}

	public function testRegisterFail1 (): void {
		$_SERVER['MW_TESTING'] = true;
		$contract = 'invalid';
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => $contract,
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];
		$service = new Service();
		$response = $service->init($configuration);
		$this->assertEmpty($response);

		try{
			$service->register();
		}
		catch (\Exception $e) {
			$this->assertEquals("Missing/Invalid Service Contract!", $e->getMessage());
		}
	}

	public function testRegisterFail2 (): void {
		$_SERVER['MW_TESTING'] = true;
		$contract = (object)[
			"name" => "dummyserver",
			"group" => "testing",
			"version" => 1,
			"ports" => (object)[
				"data" => 7001,
				"maintenance" => 7001
			],
			"apis" => (object)[
				"main" => (object)[
					"get" => (object)[
						"/test" => (object)[
							"label" => "Test API",
							"access" => false
						]
					]
				]
			]
		];
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => $contract,
			'ip' => "false",
			'type' => 'symfony'
		];
		$service = new Service();
		$response = $service->init($configuration);
		$this->assertEmpty($response);

		try{
			$service->register();
		}
		catch (\Exception $e) {
			$this->assertEquals("Missing/Invalid IP Address Value!", $e->getMessage());
		}
	}

	public function testRegisterValidKubernetesFailedGateway (): void {
		$_SERVER['MW_TESTING'] = true;
		$contract = (object)[
			"name" => "dummyserver",
			"group" => "testing",
			"version" => 1,
			"ports" => (object)[
				"data" => 7001,
				"maintenance" => 7001
			],
			"apis" => (object)[
				"main" => (object)[
					"get" => (object)[
						"/test" => (object)[
							"label" => "Test API",
							"access" => false
						]
					]
				]
			]
		];
		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => $contract,
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$_SERVER["MW_TESTING"] = true;
		$_SERVER["GATEWAY_MAINTENANCE_IP"] = "127.0.0.1";
		$service = new Service();
		$response = $service->init($configuration);
		$this->assertEmpty($response);

		try{
			$service->register();
		}
		catch (\Exception $e) {
			$this->assertNotEmpty($e->getMessage());
		}
	}

	public function testRegisterValidKubernetesFailedGatewayAttempts1 (): void {
		$contract = (object)[
			"name" => "dummyserver",
			"group" => "testing",
			"version" => 1,
			"ports" => (object)[
				"data" => 7001,
				"maintenance" => 7001
			],
			"apis" => (object)[
				"main" => (object)[
					"get" => (object)[
						"/test" => (object)[
							"label" => "Test API",
							"access" => false
						]
					]
				]
			]
		];
		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => $contract,
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$_SERVER["MW_TESTING"] = true;
		$_SERVER["GATEWAY_MAINTENANCE_IP"] = "127.0.0.1";
		$service = new Service();

		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();
		$stub1->curl_error = false;
		$stub1->method('getResponse')->willReturn(json_encode([
			'result' => false,
			'errors' => [
				'101' => [
					'code' => 101,
					'message' => 'Dummy Error!'
				]
			]
		]));

		$response = $service->init($configuration, $stub1);
		$this->assertEmpty($response);

		try{
			$service->register();
		}
		catch (\Exception $e) {
			$this->assertNotEmpty($e->getMessage());
		}
	}

	public function testRegisterValidKubernetesFailedGatewayAttempts2 (): void {
		$contract = (object)[
			"name" => "dummyserver",
			"group" => "testing",
			"version" => 1,
			"ports" => (object)[
				"data" => 7001,
				"maintenance" => 7001
			],
			"apis" => (object)[
				"main" => (object)[
					"get" => (object)[
						"/test" => (object)[
							"label" => "Test API",
							"access" => false
						]
					]
				]
			]
		];
		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => $contract,
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$_SERVER["MW_TESTING"] = true;
		$_SERVER["GATEWAY_MAINTENANCE_IP"] = "127.0.0.1";
		$service = new Service();

		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();
		$stub1->curl_error = false;
		$stub1->method('getResponse')->willReturn(json_encode([
			'result' => false,
			'statusCode' => 101,
			'code' => 101,
			'message' => 'Dummy Error!',
			'errors' => [
				'101' => [
					'code' => 101,
					'message' => 'Dummy Error!'
				]
			]
		]));

		$response = $service->init($configuration, $stub1);
		$this->assertEmpty($response);

		try{
			$service->register();
		}
		catch (\Exception $e) {
			$this->assertNotEmpty($e->getMessage());
		}
	}

	public function testRegisterValidKubernetesWorks (): void {
		$contract = (object)[
			"name" => "dummyserver",
			"group" => "testing",
			"version" => 1,
			"ports" => (object)[
				"data" => 7001,
				"maintenance" => 7001
			],
			"apis" => (object)[
				"main" => (object)[
					"get" => (object)[
						"/test" => (object)[
							"label" => "Test API",
							"access" => false
						]
					]
				]
			]
		];
		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => $contract,
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$contract2 = json_decode(json_encode($contract));
		$contract2->ports->data = $configuration->platformOptions['exposedPort'];
		$contract2->ports->maintenance = $configuration->platformOptions['exposedPort'];
		$contract2->host = (object)[
			"weight" => 1,
			"hostname" => $configuration->ip,
			"ip" => $configuration->ip
		];

		$_SERVER["MW_TESTING"] = true;
		$_SERVER["GATEWAY_MAINTENANCE_IP"] = "127.0.0.1";
		$service = new Service();
		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();
		$stub1->curl_error = false;
		$stub1->method('getResponse')->willReturn(json_encode([
			'result' => true,
			'data' => json_encode($contract2)
		]));

		$response = $service->init($configuration, $stub1);
		$this->assertEmpty($response);

		$response = $service->register();
		$this->assertNotEmpty($response);
		$this->assertEquals($response, $contract2);
	}

	public function testGetFail1 (): void {
		$contract = 'invalid';
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => $contract,
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];
		$service = new Service();
		$response = $service->init($configuration);
		$this->assertEmpty($response);

		try{
			$service->get();
		}
		catch (\Exception $e) {
			$this->assertEquals("Missing/Invalid Service Contract!", $e->getMessage());
		}
	}

	public function testGetValidKubernetesFailedGateway (): void {
		$contract = (object)[
			"name" => "dummyserver",
			"group" => "testing",
			"version" => 1,
			"ports" => (object)[
				"data" => 7001,
				"maintenance" => 7001
			],
			"apis" => (object)[
				"main" => (object)[
					"get" => (object)[
						"/test" => (object)[
							"label" => "Test API",
							"access" => false
						]
					]
				]
			]
		];
		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => $contract,
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$_SERVER["MW_TESTING"] = true;
		$_SERVER["GATEWAY_MAINTENANCE_IP"] = "127.0.0.1";
		$service = new Service();
		$response = $service->init($configuration);
		$this->assertEmpty($response);

		try{
			$service->get();
		}
		catch (\Exception $e) {
			$this->assertNotEmpty($e->getMessage());
		}
	}

	public function testGetValidKubernetesFailedGatewayAttempts1 (): void {
		$contract = (object)[
			"name" => "dummyserver",
			"group" => "testing",
			"version" => 1,
			"ports" => (object)[
				"data" => 7001,
				"maintenance" => 7001
			],
			"apis" => (object)[
				"main" => (object)[
					"get" => (object)[
						"/test" => (object)[
							"label" => "Test API",
							"access" => false
						]
					]
				]
			]
		];
		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => $contract,
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$_SERVER["MW_TESTING"] = true;
		$_SERVER["GATEWAY_MAINTENANCE_IP"] = "127.0.0.1";
		$service = new Service();

		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();
		$stub1->curl_error = false;
		$stub1->method('getResponse')->willReturn(json_encode([
			'result' => false,
			'errors' => [
				'101' => [
					'code' => 101,
					'message' => 'Dummy Error!'
				]
			]
		]));

		$response = $service->init($configuration, $stub1);
		$this->assertEmpty($response);

		try{
			$service->get();
		}
		catch (\Exception $e) {
			$this->assertEquals("Error 101 : Dummy Error!\n", $e->getMessage());
		}
	}

	public function testGetValidKubernetesFailedGatewayAttempts2 (): void {
		$contract = (object)[
			"name" => "dummyserver",
			"group" => "testing",
			"version" => 1,
			"ports" => (object)[
				"data" => 7001,
				"maintenance" => 7001
			],
			"apis" => (object)[
				"main" => (object)[
					"get" => (object)[
						"/test" => (object)[
							"label" => "Test API",
							"access" => false
						]
					]
				]
			]
		];
		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => $contract,
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$_SERVER["MW_TESTING"] = true;
		$_SERVER["GATEWAY_MAINTENANCE_IP"] = "127.0.0.1";
		$service = new Service();

		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();
		$stub1->curl_error = false;
		$stub1->method('getResponse')->willReturn(json_encode([
			'result' => false,
			'statusCode' => 101,
			'code' => 101,
			'message' => 'Dummy Error!',
			'errors' => [
				'101' => [
					'code' => 101,
					'message' => 'Dummy Error!'
				]
			]
		]));

		$response = $service->init($configuration, $stub1);
		$this->assertEmpty($response);

		try{
			$service->get();
		}
		catch (\Exception $e) {
			$this->assertEquals("101", $e->getCode());
			$this->assertEquals("Dummy Error!", $e->getMessage());
		}
	}

	public function testGetValidKubernetesWorks (): void {
		$contract = (object)[
			"name" => "dummyserver",
			"group" => "testing",
			"version" => 1,
			"ports" => (object)[
				"data" => 7001,
				"maintenance" => 7001
			],
			"apis" => (object)[
				"main" => (object)[
					"get" => (object)[
						"/test" => (object)[
							"label" => "Test API",
							"access" => false
						]
					]
				]
			]
		];
		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => $contract,
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$contract2 = json_decode(json_encode($contract));
		$contract2->ports->data = $configuration->platformOptions['exposedPort'];
		$contract2->ports->maintenance = $configuration->platformOptions['exposedPort'];
		$contract2->host = (object)[
			"weight" => 1,
			"hostname" => $configuration->ip,
			"ip" => $configuration->ip
		];

		$_SERVER["MW_TESTING"] = true;
		$_SERVER["GATEWAY_MAINTENANCE_IP"] = "127.0.0.1";
		$service = new Service();
		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();
		$stub1->curl_error = false;
		$stub1->method('getResponse')->willReturn(json_encode([
			'result' => true,
			'data' => json_encode($contract2)
		]));

		$response = $service->init($configuration, $stub1);
		$this->assertEmpty($response);

		$response = $service->get();
		$this->assertNotEmpty($response);
		$this->assertEquals($response, $contract2);
	}
}