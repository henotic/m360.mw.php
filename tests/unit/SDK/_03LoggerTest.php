<?php declare(strict_types=1);

use M360\Logger;
use PHPUnit\Framework\TestCase;

/**
 * @covers M360\Logger
 */
final class _03LoggerTest extends TestCase
{
	public function testExtendedLoggerConfiguration1 (): void {

		$logger = new Logger((object)[
			'logger' => [
				"format" => (object)[
					"levelInString" => true,
		            "outputMode" => "long"
		        ],
		        "level" => "debug",
		        "src" => true,
		        "streams" => [
					(object)[
			            "level" => "error",
		                "stream" => "process.stdout"
		            ]
		        ],
		        "serializers" => (object)[
					"req" => "function(req){ return { method: req.method, url: req.url }; }",
		            "res" => ""
		        ]
			]
		]);

		$this->assertNotEmpty($logger->get());
	}

	public function testExtendedLoggerConfiguration2 (): void {

		$logger = new Logger((object)[
			'logger' => [
				"format" => (object)[
					"levelInString" => true,
					"outputMode" => "long"
				],
				"level" => "debug",
				"src" => true,
				"streams" => [
					(object)[
						"level" => "error",
						"path" => __FILE__ . "/phpmw.log"
					]
				],
				"serializers" => (object)[
					"req" => "function(req){ return { method: req.method, url: req.url }; }",
					"res" => ""
				]
			]
		]);

		$this->assertNotEmpty($logger->get());
	}
}