<?php declare(strict_types=1);

use M360\Tenant;
use PHPUnit\Framework\TestCase;

/**
 * @covers M360\Tenant
 */
final class _04TenantTest extends TestCase
{
	public function testSetGatewayConnector (): void {

		$tenant = new Tenant();
		$gwConnect = new \M360\gateway\Connector("myService", 1);
		$tenant->setGatewayConnector($gwConnect);
		$this->assertNotEmpty($tenant);
	}

	public function testGetReturnsNull (): void {
		$tenant = new Tenant();
		$myTenant = $tenant->get((object)[], "");
		$this->assertEmpty($myTenant);
	}

	public function testGetReturnsNoM360Null (): void {
		$tenant = new Tenant();
		$myTenant = $tenant->get([], "MIKE");
		$this->assertEmpty($myTenant);
	}

	public function testGetReturnsNotFoundNull (): void {
		$tenant = new Tenant();
		$myTenant = $tenant->get([
			'm360' => json_encode((object)[
				'tenants' => [
					(object)[
						'code' => 'TNT1'
					]
				]
			])
		], "MIKE");
		$this->assertEmpty($myTenant);
	}

	public function testGetReturnsFound (): void {
		$tenant = new Tenant();
		$myTenant = $tenant->get([
			'm360' => json_encode((object)[
				'tenants' => [
					(object)[
						'code' => 'MIKE',
						'id' => 1,
						'description' => 'Test Tenant'
					]
				]
			])
		], "MIKE");
		$this->assertNotEmpty($myTenant);
		$this->assertEquals($myTenant, (object)[
			'code' => 'MIKE',
			'id' => 1,
			'description' => 'Test Tenant'
		]);
	}

	public function testListReturnsNoM360Null (): void {
		$tenant = new Tenant();
		$myTenants = $tenant->list([]);
		$this->assertEmpty($myTenants);
	}

	public function testListReturnsFound (): void {
		$tenant = new Tenant();
		$myTenants = $tenant->list([
			'm360' => json_encode((object)[
				'tenants' => [
					(object)[
						'code' => 'MIKE',
						'id' => 1,
						'description' => 'Test Tenant'
					]
				]
			])
		], "MIKE");
		$this->assertNotEmpty($myTenants);
		$this->assertEquals($myTenants, [
			(object)[
				'code' => 'MIKE',
				'id' => 1,
				'description' => 'Test Tenant'
			]
		]);
	}

	public function testFindReturnsOK (): void {
		$tenant = new Tenant();

		$stub1 = $this->createStub(M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn(['TNT1', 'TNT2']);
		$tenant->setGatewayConnector($stub1);

		$myTenants = $tenant->find(['TNT1', 'TNT2']);
		$this->assertNotEmpty($myTenants);
		$this->assertEquals($myTenants, ['TNT1', 'TNT2']);
	}
}