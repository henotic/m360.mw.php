<?php declare(strict_types=1);

use M360\Awareness;
use M360\User;
use PHPUnit\Framework\TestCase;

/**
 * @covers M360\Awareness
 * @covers M360\M360Cache
 */
final class _06AwarenessTest extends TestCase
{
	public function testInitResetSetGtwConnector (): void {

		$awareness = new Awareness();
		$awareness->init(2);
		$awareness->resetTTL(1);
		$gwConnect = new \M360\gateway\Connector("myService", 1);
		$awareness->setGatewayConnector($gwConnect);
		$this->assertNotEmpty($awareness);
	}

	public function testGetReturnsOKNoCacheControl (): void {

		$awareness = new Awareness();
		$awareness->init(2);

		$stub1 = $this->createStub(M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn((object)[
			'myService' => (object)[
				"name" => "myService",
				"version" => "1",
				"ip" => "127.0.0.1",
				"ports" => (object)[
					'data' => 80,
					'maintenance' => 80
				]
			]
		]);
		$awareness->setGatewayConnector($stub1);

		$myAwareness = $awareness->get('127.0.0.1', 80, ['Cache-Control' => 'no-cache']);
		$this->assertNotEmpty($myAwareness);
		$this->assertEquals($myAwareness, (object)[
			'myService' => (object)[
				"name" => "myService",
				"version" => "1",
				"ip" => "127.0.0.1",
				"ports" => (object)[
					'data' => 80,
					'maintenance' => 80
				]
			]
		]);
	}

	public function testGetReturnsOKWithCacheControl (): void {

		$awareness = new Awareness();
		$awareness->init(200);

		$stub1 = $this->createStub(M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn((object)[
			'myService' => (object)[
				"name" => "myService",
				"version" => "1",
				"ip" => "127.0.0.1",
				"ports" => (object)[
					'data' => 80,
					'maintenance' => 80
				]
			]
		]);
		$awareness->setGatewayConnector($stub1);

		$myAwareness = $awareness->get('127.0.0.1', 80);
		$this->assertNotEmpty($myAwareness);
		$this->assertEquals($myAwareness, (object)[
			'myService' => (object)[
				"name" => "myService",
				"version" => "1",
				"ip" => "127.0.0.1",
				"ports" => (object)[
					'data' => 80,
					'maintenance' => 80
				]
			]
		]);
	}

	public function testGetReturnsOKWithCacheControlReturnsFromCache (): void {

		$awareness = new Awareness();
		$awareness->init(200);

		$stub1 = $this->createStub(M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn((object)[
			'myService' => (object)[
				"name" => "myService",
				"version" => "1",
				"ip" => "127.0.0.1",
				"ports" => (object)[
					'data' => 80,
					'maintenance' => 80
				]
			]
		]);
		$awareness->setGatewayConnector($stub1);

		$myAwareness1 = $awareness->get('127.0.0.1', 80);
		$this->assertNotEmpty($myAwareness1);

		$myAwareness2 = $awareness->get('127.0.0.1', 80);
		$this->assertNotEmpty($myAwareness2);
		$this->assertEquals($myAwareness2, (object)[
			'myService' => (object)[
				"name" => "myService",
				"version" => "1",
				"ip" => "127.0.0.1",
				"ports" => (object)[
					'data' => 80,
					'maintenance' => 80
				]
			]
		]);
	}

	public function testReloadAndGetReturnsOKWithNoCache (): void {

		$awareness = new Awareness();
		$awareness->init(200);

		$stub1 = $this->createStub(M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn((object)[
			'myService' => (object)[
				"name" => "myService",
				"version" => "1",
				"ip" => "127.0.0.1",
				"ports" => (object)[
					'data' => 80,
					'maintenance' => 80
				]
			]
		]);
		$awareness->setGatewayConnector($stub1);

		$myAwareness1 = $awareness->get('127.0.0.1', 80);
		$this->assertNotEmpty($myAwareness1);

		$myAwareness2 = $awareness->reload('127.0.0.1', 80);
		$this->assertNotEmpty($myAwareness2);
		$this->assertEquals($myAwareness2, (object)[
			'myService' => (object)[
				"name" => "myService",
				"version" => "1",
				"ip" => "127.0.0.1",
				"ports" => (object)[
					'data' => 80,
					'maintenance' => 80
				]
			]
		]);
	}

	public function testGetReturnsInvalidResponse (): void {

		$awareness = new Awareness();
		$awareness->init(2);

		$stub1 = $this->createStub(M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn((object)[
			'error' => 101,
			'message' => 'Dummy Error!'
		]);
		$awareness->setGatewayConnector($stub1);

		$myAwareness = $awareness->get('127.0.0.1', 80);
		$this->assertNotEmpty($myAwareness);
		$this->assertEquals($myAwareness, "Error: 101 => Dummy Error!");
	}

	public function testGetNextHostReturnsOk (): void {

		$awareness = new Awareness();
		$awareness->init(200);

		$stub1 = $this->createStub(M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn((object)[
			'myService' => (object)[
				"name" => "myService",
				"version" => "1",
				"ip" => "127.0.0.1",
				"ports" => (object)[
					'data' => 80,
					'maintenance' => 80
				]
			]
		]);
		$awareness->setGatewayConnector($stub1);

		$myAwareness = $awareness->getNextHost("myService", 1, '127.0.0.1', 80);
		$this->assertNotEmpty($myAwareness);
		$this->assertEquals($myAwareness, (object)[
			'myService' => (object)[
				"name" => "myService",
				"version" => "1",
				"ip" => "127.0.0.1",
				"ports" => (object)[
					'data' => 80,
					'maintenance' => 80
				]
			]
		]);
	}

	public function testProxyReturnsOk (): void {

		$awareness = new Awareness();
		$awareness->init(200);

		$stub1 = $this->createStub(M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn((object)[
			"name" => "myService",
			"version" => "1",
			"ip" => "127.0.0.1",
			"port" => 80
		]);
		$awareness->setGatewayConnector($stub1);

		$myAwareness = $awareness->proxy((object)[
			'service' => 'myService',
			'method' => 'get',
			'version' => 1,
			'ip' => '127.0.0.1',
			'port' => 80,
			'route' => "/test",
			'qs' => [],
			'body' => [],
			'headers' => [],
		]);

		$this->assertNotEmpty($myAwareness);
	}

	public function testProxyReturnsOkWithUser (): void {

		$awareness = new Awareness();
		$awareness->init(200);

		$stub1 = $this->createStub(M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn((object)[
			"name" => "myService",
			"version" => "1",
			"ip" => "127.0.0.1",
			"port" => 80
		]);
		$awareness->setGatewayConnector($stub1);

		$user = new User();
		$stub2 = $this->createStub(M360\gateway\Connector::class);
		$stub2->method('invoke')->willReturn((object)[
				"_id" => "5f23b5fff05042751d6fea9d",
				"username" => "owner",
				"firstName" => "Mike",
				"lastName" => "Hajj",
				"email" => "mike@corsairm360.com",
				"status" => "active",
				"profile" => (object)[
					"gender" => "male",
					"picture" => ""
				],
				"ts" => 1614693465902,
				"account" => (object)[
					"_id" => "6041ed8e08acbf71381e0789",
					"name" => "Corsair M360 - Henotic",
					"ts" => 1614613159873
				],
				"groups" => [
					"admin"
				],
				"tenants" => [
					(object)[
						"id" => "5f2abc6d3b62d4a05c929df9",
						"code" => "M360"
					]
				],
				"config" => (object)[
					"M360" => (object)[
						"common" => (object)[
							'foo' => 'bar'
						],
						"specific" => (object)[
							'myService' => (object)[
								'foo' => 'bar'
							]
						]
					]
				],
				"auto_registration_flow" => "corsair_saas",
				"m360" => (object)[
					"user" => (object)[
						"username" => "owner",
						"firstName" => "Mike",
						"lastName" => "Hajj",
						"email" => "mike@corsairm360.com",
						"status" => "active",
						"profile" => (object)[
							"gender" => "male",
							"picture" => ""
						],
						"ts" => 1614693465902,
						"account" => (object)[
							"_id" => "6041ed8e08acbf71381e0789",
							"name" => "Corsair M360 - Henotic",
							"ts" => 1614613159873
						],
						"groups" => [
							"admin"
						],
						"auto_registration_flow" => "corsair_saas",
						"m360" => [
						],
						"id" => "5f23b5fff05042751d6fea9d"
					],
					"tenants" => [
						(object)[
							"id" => "5f2abc6d3b62d4a05c929df9",
							"code" => "M360"
						]
					]
				]
		]);
		$user->setGatewayConnector($stub2);

		$awareness->setUserModule($user);
		$myProxyContext = [
			'method' => 'get',
			'service' => 'myService',
			'version' => 1,
			'ip' => '127.0.0.1',
			'port' => 80,
			'route' => "/test",
			'qs' => [],
			'body' => [],
			'headers' => [],
			'user' => 10
		];
		$myAwareness = $awareness->proxy((object)$myProxyContext);

		$this->assertNotEmpty($myAwareness);
	}
}