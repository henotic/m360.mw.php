<?php declare(strict_types=1);

use M360\User;
use PHPUnit\Framework\TestCase;

/**
 * @covers M360\User
 */
final class _05UserTest extends TestCase
{
	public function testSetGatewayConnector (): void {

		$user = new User();
		$gwConnect = new \M360\gateway\Connector("myService", 1);
		$user->setGatewayConnector($gwConnect);
		$this->assertNotEmpty($user);
	}

	public function testGetReturnsNull (): void {
		$user = new User();
		$myUser = $user->get([]);
		$this->assertEmpty($myUser);
	}

	public function testGetReturnsNoM360Null (): void {
		$user = new User();
		$myUser = $user->get([]);
		$this->assertEmpty($myUser);
	}

	public function testGetReturnsFound (): void {
		$user = new User();
		$myUser = $user->get([
			'm360' => json_encode((object)[
				'user' => (object)[
					'username' => 'Mike'
				]
			])
		]);
		$this->assertNotEmpty($myUser);
		$this->assertEquals($myUser, (object)[
			'username' => 'Mike'
		]);
	}

	public function testFindReturnsOK (): void {
		$user = new User();

		$stub1 = $this->createStub(M360\gateway\Connector::class);
		$stub1->method('invoke')->willReturn([
			(object)[
				'username' => 'mikehajj',
				'firstName' => 'mike',
				'lastName' => 'hajj'
			],
			(object)[
				'username' => 'keithbacon',
				'firstName' => 'keith',
				'lastName' => 'bacon'
			]
		]);
		$user->setGatewayConnector($stub1);

		$myUsers = $user->find([
			'id' => 1,
			'start' => 2,
			'limit' => 2,
			'group' => ['admins'],
			'service' => 'consoleapi',
			'version' => 1,
			'tenant' => ['ADMINS'],
		]);
		$this->assertNotEmpty($myUsers);

		$this->assertEquals($myUsers, [
			(object)[
				'username' => 'mikehajj',
				'firstName' => 'mike',
				'lastName' => 'hajj'
			],
			(object)[
				'username' => 'keithbacon',
				'firstName' => 'keith',
				'lastName' => 'bacon'
			]
		]);
	}
}