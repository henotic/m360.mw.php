<?php declare(strict_types=1);

use M360\gateway\Connector;
use PHPUnit\Framework\TestCase;

/**
 * @covers M360\gateway\Connector
 */
final class _09GatewayConnectorTest extends TestCase
{
	public function testFail1 (): void {
		$gtwConnector = new Connector('myServide', 1);

		try {
			$gtwConnector->invoke([
				'ip' => '127.0.0.1',
				'port' => '5000',
				'headers' => [
					'Content-Type' => 'application/json'
				],
				'method' => 'get',
				'route' => '/registry/get',
				'qs' => [
					'foo' => 'bar'
				],
				'body' => [
					'data' => 'foobar'
				],
			]);
		} catch (\Exception $e) {
			$this->assertNotEmpty($e->getMessage());
		}

	}

	public function testFail2 (): void {
		$gtwConnector = new Connector('myServide', 1);

		try {
			$gtwConnector->invoke([
				'ip' => '127.0.0.1',
				'port' => '5000',
				'headers' => [
					'Content-Type' => 'application/json'
				],
				'method' => 'post',
				'route' => '/registry/get',
				'qs' => [
					'foo' => 'bar'
				],
				'body' => [
					'data' => 'foobar'
				],
				'retryOnFailure' => 3,
				'retryDelay' => 500
			]);
		} catch (\Exception $e) {
			$this->assertNotEmpty($e->getMessage());
		}

	}

	public function testFail3InvalidResponse (): void {

		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();
		$stub1->curl_error = false;
		$stub1->method('getResponse')->willReturn(json_encode([
			'result' => false,
			'errors' => [
				'101' => [
					'code' => 101,
					'message' => 'Dummy Error!'
				]
			]
		]));
		$gtwConnector = new Connector('myServide', 1, '127.0.0.1', 5000, false, $stub1);

		try {
			$gtwConnector->invoke([
				'ip' => '127.0.0.1',
				'port' => '5000',
				'headers' => [
					'Content-Type' => 'application/json'
				],
				'method' => 'post',
				'route' => '/registry/get',
				'qs' => [
					'foo' => 'bar'
				],
				'body' => [
					'data' => 'foobar'
				],
				'retryOnFailure' => 3,
				'retryDelay' => 500
			]);
		} catch (\Exception $e) {
			$this->assertEquals("Error 101 : Dummy Error!\n", $e->getMessage());
		}

	}

	public function testFail3InvalidResponse2 (): void {

		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();
		$stub1->curl_error = false;
		$stub1->method('getResponse')->willReturn(json_encode([
			'result' => false,
			'error' => 101,
			'message' => 'Dummy Error!',
			'errors' => [
				'101' => [
					'code' => 101,
					'message' => 'Dummy Error!'
				]
			]
		]));
		$gtwConnector = new Connector('myServide', 1, '127.0.0.1', 5000, false, $stub1);

		try {
			$gtwConnector->invoke([
				'ip' => '127.0.0.1',
				'port' => '5000',
				'headers' => [
					'Content-Type' => 'application/json'
				],
				'method' => 'post',
				'route' => '/registry/get',
				'qs' => [
					'foo' => 'bar'
				],
				'body' => [
					'data' => 'foobar'
				],
				'retryOnFailure' => 3,
				'retryDelay' => 500
			]);
		} catch (\Exception $e) {
			$this->assertEquals("Dummy Error!,Error 101 : Dummy Error!\n", $e->getMessage());
		}

	}

	public function testValid1 (): void {
		$data = (object)[
			'foo' => 'bar'
		];

		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();
		$stub1->curl_error = false;
		$stub1->method('getResponse')->willReturn(json_encode([
			'result' => true,
			'data' => json_encode($data)
		]));
		$gtwConnector = new Connector('myServide', 1, '127.0.0.1', 5000, false, $stub1);

		$response = $gtwConnector->invoke([
			'ip' => '127.0.0.1',
			'port' => '5000',
			'headers' => [
				'Content-Type' => 'application/json'
			],
			'method' => 'post',
			'route' => '/registry/get',
			'qs' => [
				'foo' => 'bar'
			],
			'body' => [
				'data' => 'foobar'
			],
			'retryOnFailure' => 3,
			'retryDelay' => 500
		]);
		$this->assertNotEmpty($response);
		$this->assertEquals($response, $data);
	}

	public function testValid2 (): void {
		$data = (object)[
			'foobar'
		];

		$stub1 = $this->getMockBuilder(\Curl\Curl::class)->getMock();
		$stub1->curl_error = false;
		$stub1->method('getResponse')->willReturn(json_encode($data));
		$gtwConnector = new Connector('myServide', 1, '127.0.0.1', 5000, false, $stub1);

		$response = $gtwConnector->invoke([
			'ip' => '127.0.0.1',
			'port' => '5000',
			'headers' => [],
			'method' => 'post',
			'route' => '/registry/get',
			'qs' => [
				'foo' => 'bar'
			],
			'body' => [
				'data' => 'foobar'
			],
			'retryOnFailure' => 3,
			'retryDelay' => 500,
            'ssl' => false
		]);
		$this->assertNotEmpty($response);
		$this->assertEquals($response, $data);
	}
}