<?php declare(strict_types=1);

use M360\M360Cache;
use PHPUnit\Framework\TestCase;

/**
 * @covers M360\M360Cache
 */
final class _07CacheTest extends TestCase
{
	public function testExtendedCacheM360 (): void {

		$myCache = new M360Cache(1);
		$myCache->set('mike', 'hajj');
		sleep(3);
		$myCache->check_ttls();
		$cacheResponse = $myCache->get('mike');
		$this->assertEmpty($cacheResponse);
	}

}