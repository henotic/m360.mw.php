<?php declare(strict_types=1);

use M360\Maintenance\Maintenance;
use PHPUnit\Framework\TestCase;

class SymfonyQuery {

	private $params;

	public function __construct($queryParams){
		$this->params = $queryParams;
	}

	public function get($paramName){
		return !empty($this->params[$paramName]) ? $this->params[$paramName] : [];
	}
}

class SymfonyRequest{

	public $query;

	public function __construct($queryParams){
		$this->query = new SymfonyQuery($queryParams);
	}
}

class LaravelRequest{

	public $sQuery;

	public function __construct($queryParams){
		$this->sQuery = new SymfonyQuery($queryParams);
	}

	public function query(){
		return $this->sQuery;
	}
}
/**
 * @covers M360\Maintenance\Maintenance
 */
final class _10MaintenanceTest extends TestCase
{
	public function testReloadRegistryAndAwarenessReturnsFalse (): void {

		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$stub1 = $this->createStub(M360\Registry::class);
		$stub1->method('reload')->willReturn(null);

		$stub2 = $this->createStub(M360\Service::class);

		$stub3 = $this->createStub(M360\Awareness::class);
		$stub3->method('reload')->willReturn(null);

		$maintenance = new Maintenance('mikePHPAPP', $configuration, $stub1, $stub2, $stub3);

		$registry = $maintenance->reloadRegistry([]);
		$this->assertEmpty($registry);

		$awareness = $maintenance->reloadAwareness([]);
		$this->assertEmpty($awareness);
	}

	public function testReloadRegistryAndAwarenessNoIgnoreList (): void {

		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];
		$gtwAwareness = (object)[
			"abcbridgeapi" => (object)[
				"v1" => (object)[
					"_id" => "61a9bd2ff51d80259f0ea2a0",
					"name" => "abcbridgeapi",
					"version" => (object)[
						"value" => 1,
						"latest" => true
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/user/" => (object)[
									"access" => false,
									"label" => "Get User Information"
								]
							]
						],
						"maintenance" => (object)[
							"get" => (object)[
								"/model/cache/clear" => (object)[
									"label" => "Clear Data Model Cache",
									"access" => false

								]
							]
						]
					],
					"group" => "ABCAPI",
					"hash" => "4e63bded-e1c6-413c-a5ed-7b9b4752e551",
					"heartbeat" => (object)[
						"method" => "get",
						"route" => "/heartbeat",
						"access" => false,
						"label" => "Heartbeat"
					],
					"ports" => (object)[
						"data" => 4300,
						"maintenance" => 4300
					],
					"registry" => (object)[
						"method" => "get",
						"route" => "/registry/reload",
						"access" => false,
						"label" => "Reload Registry"
					],
					"stats" => "http://127.0.0.1:4300/stats/ux#",
					"swagger" => "http://127.0.0.1:4300/documentation",
					"weight" => 1,
					"host" => (object)[
						"127.0.0.1:4300" => (object)[
							"hostname" => "M360-staging-001",
							"ip" => "127.0.0.1",
							"port" => 4300,
							"maintenance" => 4300,
							"weight" => 1,
							"lastCheck" => 1651060396883,
							"healthy" => true
						]
					],
					"healthy" => true,
					"lastCheck" => 1651060396883
				],
				"latest" => "v1"
			]
		];

		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$request = (object)[];

		$stub1 = $this->createStub(M360\Registry::class);
		$stub1->method('reload')->willReturn($gtwRegistry);

		$stub2 = $this->createStub(M360\Service::class);

		$stub3 = $this->createStub(M360\Awareness::class);
		$stub3->method('reload')->willReturn($gtwAwareness);

		$maintenance = new Maintenance('mikePHPAPP', $configuration, $stub1, $stub2, $stub3);

		$registry = $maintenance->reloadRegistry($request);
		$this->assertNotEmpty($registry);

		$awareness = $maintenance->reloadAwareness($request);
		$this->assertNotEmpty($awareness);
	}

	public function testReloadRegistryAndAwarenessWithIgnoreListSymfony (): void {

		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];
		$gtwAwareness = (object)[
			"abcbridgeapi" => (object)[
				"v1" => (object)[
					"_id" => "61a9bd2ff51d80259f0ea2a0",
					"name" => "abcbridgeapi",
					"version" => (object)[
						"value" => 1,
						"latest" => true
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/user/" => (object)[
									"access" => false,
									"label" => "Get User Information"
								]
							]
						],
						"maintenance" => (object)[
							"get" => (object)[
								"/model/cache/clear" => (object)[
									"label" => "Clear Data Model Cache",
									"access" => false

								]
							]
						]
					],
					"group" => "ABCAPI",
					"hash" => "4e63bded-e1c6-413c-a5ed-7b9b4752e551",
					"heartbeat" => (object)[
						"method" => "get",
						"route" => "/heartbeat",
						"access" => false,
						"label" => "Heartbeat"
					],
					"ports" => (object)[
						"data" => 4300,
						"maintenance" => 4300
					],
					"registry" => (object)[
						"method" => "get",
						"route" => "/registry/reload",
						"access" => false,
						"label" => "Reload Registry"
					],
					"stats" => "http://127.0.0.1:4300/stats/ux#",
					"swagger" => "http://127.0.0.1:4300/documentation",
					"weight" => 1,
					"host" => (object)[
						"127.0.0.1:4300" => (object)[
							"hostname" => "M360-staging-001",
							"ip" => "127.0.0.1",
							"port" => 4300,
							"maintenance" => 4300,
							"weight" => 1,
							"lastCheck" => 1651060396883,
							"healthy" => true
						]
					],
					"healthy" => true,
					"lastCheck" => 1651060396883
				],
				"latest" => "v1"
			]
		];

		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$request = new SymfonyRequest(['ignoreList' => ['127.0.0.3']]);

		$stub1 = $this->createStub(M360\Registry::class);
		$stub1->method('reload')->willReturn($gtwRegistry);

		$stub2 = $this->createStub(M360\Service::class);

		$stub3 = $this->createStub(M360\Awareness::class);
		$stub3->method('reload')->willReturn($gtwAwareness);

		$maintenance = new Maintenance('mikePHPAPP', $configuration, $stub1, $stub2, $stub3);

		$registry = $maintenance->reloadRegistry($request);
		$this->assertNotEmpty($registry);

		$awareness = $maintenance->reloadAwareness($request);
		$this->assertNotEmpty($awareness);
	}

	public function testReloadRegistryAndAwarenessWithIgnoreListLaravel (): void {

		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];
		$gtwAwareness = (object)[
			"abcbridgeapi" => (object)[
				"v1" => (object)[
					"_id" => "61a9bd2ff51d80259f0ea2a0",
					"name" => "abcbridgeapi",
					"version" => (object)[
						"value" => 1,
						"latest" => true
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/user/" => (object)[
									"access" => false,
									"label" => "Get User Information"
								]
							]
						],
						"maintenance" => (object)[
							"get" => (object)[
								"/model/cache/clear" => (object)[
									"label" => "Clear Data Model Cache",
									"access" => false

								]
							]
						]
					],
					"group" => "ABCAPI",
					"hash" => "4e63bded-e1c6-413c-a5ed-7b9b4752e551",
					"heartbeat" => (object)[
						"method" => "get",
						"route" => "/heartbeat",
						"access" => false,
						"label" => "Heartbeat"
					],
					"ports" => (object)[
						"data" => 4300,
						"maintenance" => 4300
					],
					"registry" => (object)[
						"method" => "get",
						"route" => "/registry/reload",
						"access" => false,
						"label" => "Reload Registry"
					],
					"stats" => "http://127.0.0.1:4300/stats/ux#",
					"swagger" => "http://127.0.0.1:4300/documentation",
					"weight" => 1,
					"host" => (object)[
						"127.0.0.1:4300" => (object)[
							"hostname" => "M360-staging-001",
							"ip" => "127.0.0.1",
							"port" => 4300,
							"maintenance" => 4300,
							"weight" => 1,
							"lastCheck" => 1651060396883,
							"healthy" => true
						]
					],
					"healthy" => true,
					"lastCheck" => 1651060396883
				],
				"latest" => "v1"
			]
		];

		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$request = new LaravelRequest(['ignoreList' => ['127.0.0.3']]);

		$stub1 = $this->createStub(M360\Registry::class);
		$stub1->method('reload')->willReturn($gtwRegistry);

		$stub2 = $this->createStub(M360\Service::class);

		$stub3 = $this->createStub(M360\Awareness::class);
		$stub3->method('reload')->willReturn($gtwAwareness);

		$maintenance = new Maintenance('mikePHPAPP', $configuration, $stub1, $stub2, $stub3);

		$registry = $maintenance->reloadRegistry($request);
		$this->assertNotEmpty($registry);

		$awareness = $maintenance->reloadAwareness($request);
		$this->assertNotEmpty($awareness);
	}

	public function testReloadRegistryAndAwarenessNoIgnoreListKubernetes (): void {

		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];
		$gtwAwareness = (object)[
			"abcbridgeapi" => (object)[
				"v1" => (object)[
					"_id" => "61a9bd2ff51d80259f0ea2a0",
					"name" => "abcbridgeapi",
					"version" => (object)[
						"value" => 1,
						"latest" => true
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/user/" => (object)[
									"access" => false,
									"label" => "Get User Information"
								]
							]
						],
						"maintenance" => (object)[
							"get" => (object)[
								"/model/cache/clear" => (object)[
									"label" => "Clear Data Model Cache",
									"access" => false

								]
							]
						]
					],
					"group" => "ABCAPI",
					"hash" => "4e63bded-e1c6-413c-a5ed-7b9b4752e551",
					"heartbeat" => (object)[
						"method" => "get",
						"route" => "/heartbeat",
						"access" => false,
						"label" => "Heartbeat"
					],
					"ports" => (object)[
						"data" => 4300,
						"maintenance" => 4300
					],
					"registry" => (object)[
						"method" => "get",
						"route" => "/registry/reload",
						"access" => false,
						"label" => "Reload Registry"
					],
					"stats" => "http://127.0.0.1:4300/stats/ux#",
					"swagger" => "http://127.0.0.1:4300/documentation",
					"weight" => 1,
					"host" => (object)[
						"127.0.0.1:4300" => (object)[
							"hostname" => "M360-staging-001",
							"ip" => "127.0.0.1",
							"port" => 4300,
							"maintenance" => 4300,
							"weight" => 1,
							"lastCheck" => 1651060396883,
							"healthy" => true
						]
					],
					"healthy" => true,
					"lastCheck" => 1651060396883
				],
				"latest" => "v1"
			]
		];

		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => (object)[
				"namespace" => "k8s",
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$request = (object)[];

		$stub1 = $this->createStub(M360\Registry::class);
		$stub1->method('reload')->willReturn($gtwRegistry);

		$stub2 = $this->createStub(M360\Service::class);

		$stub3 = $this->createStub(M360\Awareness::class);
		$stub3->method('reload')->willReturn($gtwAwareness);

		$stub4 = $this->createStub(Curl\Curl::class);
		$stub4->method('get')->willReturn((object)[
			"subsets" => [
				"0" => (object)[
					"addresses" => [
						"0" => (object)[
							"ip" => "127.0.0.2"
						]
					]
				]
			]
		]);
		$stub4->method('getResponse')->willReturn(true);

		$k8sToken = dirname(__FILE__) . "/k8sToken";
		file_put_contents($k8sToken, "");
		$maintenance = new Maintenance('mikePHPAPP', $configuration, $stub1, $stub2, $stub3, $stub4, $k8sToken);

		$registry = $maintenance->reloadRegistry($request);
		$this->assertEmpty($registry);

		$awareness = $maintenance->reloadAwareness($request);
		$this->assertEmpty($awareness);
		unlink($k8sToken);
	}

	public function testReloadRegistryAndAwarenessNoIgnoreListKubernetesNoToken (): void {

		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];
		$gtwAwareness = (object)[
			"abcbridgeapi" => (object)[
				"v1" => (object)[
					"_id" => "61a9bd2ff51d80259f0ea2a0",
					"name" => "abcbridgeapi",
					"version" => (object)[
						"value" => 1,
						"latest" => true
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/user/" => (object)[
									"access" => false,
									"label" => "Get User Information"
								]
							]
						],
						"maintenance" => (object)[
							"get" => (object)[
								"/model/cache/clear" => (object)[
									"label" => "Clear Data Model Cache",
									"access" => false

								]
							]
						]
					],
					"group" => "ABCAPI",
					"hash" => "4e63bded-e1c6-413c-a5ed-7b9b4752e551",
					"heartbeat" => (object)[
						"method" => "get",
						"route" => "/heartbeat",
						"access" => false,
						"label" => "Heartbeat"
					],
					"ports" => (object)[
						"data" => 4300,
						"maintenance" => 4300
					],
					"registry" => (object)[
						"method" => "get",
						"route" => "/registry/reload",
						"access" => false,
						"label" => "Reload Registry"
					],
					"stats" => "http://127.0.0.1:4300/stats/ux#",
					"swagger" => "http://127.0.0.1:4300/documentation",
					"weight" => 1,
					"host" => (object)[
						"127.0.0.1:4300" => (object)[
							"hostname" => "M360-staging-001",
							"ip" => "127.0.0.1",
							"port" => 4300,
							"maintenance" => 4300,
							"weight" => 1,
							"lastCheck" => 1651060396883,
							"healthy" => true
						]
					],
					"healthy" => true,
					"lastCheck" => 1651060396883
				],
				"latest" => "v1"
			]
		];

		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"namespace" => "k8s",
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$request = (object)[];

		$stub1 = $this->createStub(M360\Registry::class);
		$stub1->method('reload')->willReturn($gtwRegistry);

		$stub2 = $this->createStub(M360\Service::class);

		$stub3 = $this->createStub(M360\Awareness::class);
		$stub3->method('reload')->willReturn($gtwAwareness);

		$stub4 = $this->createStub(Curl\Curl::class);
		$stub4->method('get')->willReturn((object)[
			"subsets" => [
				"0" => (object)[
					"addresses" => [
						"0" => (object)[
							"ip" => "127.0.0.2"
						]
					]
				]
			]
		]);
		$stub4->method('getResponse')->willReturn(true);

		$k8sToken = dirname(__FILE__) . "/k8sToken";
		file_put_contents($k8sToken, "TestingK8sToken");
		$maintenance = new Maintenance('mikePHPAPP', $configuration, $stub1, $stub2, $stub3, $stub4, $k8sToken);

		$registry = $maintenance->reloadRegistry($request);
		$this->assertNotEmpty($registry);

		$awareness = $maintenance->reloadAwareness($request);
		$this->assertNotEmpty($awareness);
		unlink($k8sToken);
	}

	public function testReloadRegistryAndAwarenessNoIgnoreListKubernetesNoBrothers (): void {

		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];
		$gtwAwareness = (object)[
			"abcbridgeapi" => (object)[
				"v1" => (object)[
					"_id" => "61a9bd2ff51d80259f0ea2a0",
					"name" => "abcbridgeapi",
					"version" => (object)[
						"value" => 1,
						"latest" => true
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/user/" => (object)[
									"access" => false,
									"label" => "Get User Information"
								]
							]
						],
						"maintenance" => (object)[
							"get" => (object)[
								"/model/cache/clear" => (object)[
									"label" => "Clear Data Model Cache",
									"access" => false

								]
							]
						]
					],
					"group" => "ABCAPI",
					"hash" => "4e63bded-e1c6-413c-a5ed-7b9b4752e551",
					"heartbeat" => (object)[
						"method" => "get",
						"route" => "/heartbeat",
						"access" => false,
						"label" => "Heartbeat"
					],
					"ports" => (object)[
						"data" => 4300,
						"maintenance" => 4300
					],
					"registry" => (object)[
						"method" => "get",
						"route" => "/registry/reload",
						"access" => false,
						"label" => "Reload Registry"
					],
					"stats" => "http://127.0.0.1:4300/stats/ux#",
					"swagger" => "http://127.0.0.1:4300/documentation",
					"weight" => 1,
					"host" => (object)[
						"127.0.0.1:4300" => (object)[
							"hostname" => "M360-staging-001",
							"ip" => "127.0.0.1",
							"port" => 4300,
							"maintenance" => 4300,
							"weight" => 1,
							"lastCheck" => 1651060396883,
							"healthy" => true
						]
					],
					"healthy" => true,
					"lastCheck" => 1651060396883
				],
				"latest" => "v1"
			]
		];

		$configuration = (object)[
			'platform' => 'kubernetes',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"namespace" => "k8s",
				"service" => "mike",
				"exposedPort" => 80
			]
		];

		$request = (object)[];

		$stub1 = $this->createStub(M360\Registry::class);
		$stub1->method('reload')->willReturn($gtwRegistry);

		$stub2 = $this->createStub(M360\Service::class);

		$stub3 = $this->createStub(M360\Awareness::class);
		$stub3->method('reload')->willReturn($gtwAwareness);

		$stub4 = $this->createStub(Curl\Curl::class);
		$stub4->method('get')->willReturn(null);
		$stub4->method('getResponse')->willReturn(true);

		$k8sToken = dirname(__FILE__) . "/k8sToken";
		file_put_contents($k8sToken, "TestingK8sToken");
		$maintenance = new Maintenance('mikePHPAPP', $configuration, $stub1, $stub2, $stub3, $stub4, $k8sToken);

		$registry = $maintenance->reloadRegistry($request);
		$this->assertNotEmpty($registry);

		$awareness = $maintenance->reloadAwareness($request);
		$this->assertNotEmpty($awareness);
		unlink($k8sToken);
	}

	public function testReloadRegistryAndAwarenessNoIgnoreListDocker (): void {

		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];
		$gtwAwareness = (object)[
			"abcbridgeapi" => (object)[
				"v1" => (object)[
					"_id" => "61a9bd2ff51d80259f0ea2a0",
					"name" => "abcbridgeapi",
					"version" => (object)[
						"value" => 1,
						"latest" => true
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/user/" => (object)[
									"access" => false,
									"label" => "Get User Information"
								]
							]
						],
						"maintenance" => (object)[
							"get" => (object)[
								"/model/cache/clear" => (object)[
									"label" => "Clear Data Model Cache",
									"access" => false

								]
							]
						]
					],
					"group" => "ABCAPI",
					"hash" => "4e63bded-e1c6-413c-a5ed-7b9b4752e551",
					"heartbeat" => (object)[
						"method" => "get",
						"route" => "/heartbeat",
						"access" => false,
						"label" => "Heartbeat"
					],
					"ports" => (object)[
						"data" => 4300,
						"maintenance" => 4300
					],
					"registry" => (object)[
						"method" => "get",
						"route" => "/registry/reload",
						"access" => false,
						"label" => "Reload Registry"
					],
					"stats" => "http://127.0.0.1:4300/stats/ux#",
					"swagger" => "http://127.0.0.1:4300/documentation",
					"weight" => 1,
					"host" => (object)[
						"127.0.0.1:4300" => (object)[
							"hostname" => "M360-staging-001",
							"ip" => "127.0.0.1",
							"port" => 4300,
							"maintenance" => 4300,
							"weight" => 1,
							"lastCheck" => 1651060396883,
							"healthy" => true
						]
					],
					"healthy" => true,
					"lastCheck" => 1651060396883
				],
				"latest" => "v1"
			]
		];

		$configuration = (object)[
			'platform' => 'docker',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"network" => "m360"
			]
		];

		$request = (object)[];

		$stub1 = $this->createStub(M360\Registry::class);
		$stub1->method('reload')->willReturn($gtwRegistry);

		$stub2 = $this->createStub(M360\Service::class);

		$stub3 = $this->createStub(M360\Awareness::class);
		$stub3->method('reload')->willReturn($gtwAwareness);

		$stub4 = $this->createStub(Curl\Curl::class);
		$stub4->method('get')->willReturn([
			(object)[
				"NetworkSettings" => (object)[
					"Networks" => [
						"m360" => (object)[
							"IPAddress" => "127.0.0.2"
						]
					]
				]
			]
		]);
		$stub4->method('getResponse')->willReturn(true);
		$maintenance = new Maintenance('mikePHPAPP', $configuration, $stub1, $stub2, $stub3, $stub4);

		$registry = $maintenance->reloadRegistry($request);
		$this->assertNotEmpty($registry);

		$awareness = $maintenance->reloadAwareness($request);
		$this->assertNotEmpty($awareness);
	}

	public function testReloadRegistryAndAwarenessNoIgnoreListDockerNoBrothers (): void {

		$gtwRegistry = (object)[
			'registry' => (object)[
				'_TTL' => 20000,
				'logger' => (object)[
					"format" => (object)[
						"levelInString" => true,
						"outputMode" => "long"
					],
					"level" => "debug",
					"src" => true,
					"streams" => [
						(object)[
							"level" => "error",
							"path" => __FILE__ . "/phpmw.log"
						]
					],
					"serializers" => (object)[
						"req" => "function(req){ return { method: req.method, url: req.url }; }",
						"res" => ""
					]
				]
			],
			'custom' => (object)[
				"mail_sendgrid" => (object)[
					"name" => "mail_sendgrid",
					"ts" => 1588140880426,
					"value" => (object)[
						"from" => "mike@mikehajj.com",
						"apiKey" => "abcdefghijklmnopqrstuvwxyz"
					]
				],
				"mail_sendgrid2" => (object)[
					"name" => "mail_sendgrid2",
					"ts" => 1588140880426,
					"value" => "abcdefghijklmnopqrstuvwxyz"
				]
			],
			'databases' => (object)[
				"single" => (object)[
					"example2" => (object)[
						"name" => "test_example2",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				],
				"multitenant" => (object)[
					"example1" => (object)[
						"name" => "test_example1",
						"cluster" => "sql_resource",
						"config" => (object)[]
					]
				]
			],
			'resources' => (object)[
				"sql_resource" => (object)[
					"servers" => [
						(object)[
							"host" => "127.0.0.1",
							"port" => 3306
						]
					],
					"credentials" => (object)[
						"username" => "",
						"password" => ""
					]
				]
			]
		];
		$gtwAwareness = (object)[
			"abcbridgeapi" => (object)[
				"v1" => (object)[
					"_id" => "61a9bd2ff51d80259f0ea2a0",
					"name" => "abcbridgeapi",
					"version" => (object)[
						"value" => 1,
						"latest" => true
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/user/" => (object)[
									"access" => false,
									"label" => "Get User Information"
								]
							]
						],
						"maintenance" => (object)[
							"get" => (object)[
								"/model/cache/clear" => (object)[
									"label" => "Clear Data Model Cache",
									"access" => false

								]
							]
						]
					],
					"group" => "ABCAPI",
					"hash" => "4e63bded-e1c6-413c-a5ed-7b9b4752e551",
					"heartbeat" => (object)[
						"method" => "get",
						"route" => "/heartbeat",
						"access" => false,
						"label" => "Heartbeat"
					],
					"ports" => (object)[
						"data" => 4300,
						"maintenance" => 4300
					],
					"registry" => (object)[
						"method" => "get",
						"route" => "/registry/reload",
						"access" => false,
						"label" => "Reload Registry"
					],
					"stats" => "http://127.0.0.1:4300/stats/ux#",
					"swagger" => "http://127.0.0.1:4300/documentation",
					"weight" => 1,
					"host" => (object)[
						"127.0.0.1:4300" => (object)[
							"hostname" => "M360-staging-001",
							"ip" => "127.0.0.1",
							"port" => 4300,
							"maintenance" => 4300,
							"weight" => 1,
							"lastCheck" => 1651060396883,
							"healthy" => true
						]
					],
					"healthy" => true,
					"lastCheck" => 1651060396883
				],
				"latest" => "v1"
			]
		];

		$configuration = (object)[
			'platform' => 'docker',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"service" => "mike",
				"network" => "m360"
			]
		];

		$request = (object)[];

		$stub1 = $this->createStub(M360\Registry::class);
		$stub1->method('reload')->willReturn($gtwRegistry);

		$stub2 = $this->createStub(M360\Service::class);

		$stub3 = $this->createStub(M360\Awareness::class);
		$stub3->method('reload')->willReturn($gtwAwareness);

		$stub4 = $this->createStub(Curl\Curl::class);
		$stub4->method('get')->willReturn(null);
		$stub4->method('getResponse')->willReturn(true);
		$maintenance = new Maintenance('mikePHPAPP', $configuration, $stub1, $stub2, $stub3, $stub4);

		$registry = $maintenance->reloadRegistry($request);
		$this->assertEmpty($registry);

		$awareness = $maintenance->reloadAwareness($request);
		$this->assertEmpty($awareness);
	}
}