<?php declare(strict_types=1);

use M360\RBAC;
use PHPUnit\Framework\TestCase;

/**
 * @covers M360\RBAC
 * @covers M360\Utils
 */
final class _11RbacTest extends TestCase
{
	public function test_init_and_get_success (): void {

		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => [
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'rbac' => (object)[
					'fields' => (object)[
						'operator' => 'allow',
						'list' => (object)[
							'first' => 'firstName',
							'username' => 'username',
							'account' => 'account',
							'config' => 'config.M360.common.pet'
						]
					],
					'resources' => (object)[
						'mode' => 'own',
						'field' => 'account',
						'value' => 'account._id'
					],
					'conditions' => (object)[
						"operator" => '$and',
						"criteria" => [
							(object)[
								'function' => "EQ",
								"arguments" => (object)[
									"field" => "username",
									"value" => "username",
									"custom" => ""
								]
							],
							(object)[
								'function' => "EQ",
								"arguments" => (object)[
									"field" => "account",
									"value" => "account._id",
									"custom" => ""
								]
							],
							(object)[
								'function' => 'EQ',
								"arguments" => (object)[
									"field" => 'username',
									"value" => 'custom',
									"custom" => 'owner'
								]
							],
							(object)[
								'function' => 'EQ',
								"arguments" => (object)[
									"field" => 'config.M360.common.primary',
									"value" => 'custom',
									"custom" => 'true'
								]
							]
						]
					],
				]
			])
		];

		$fakeCall00 = $rbac->get([]);
		$this->assertEmpty($fakeCall00);

		$fakeCall0 = $rbac->get([
			'm360' => json_encode((object)[
				'rbac' => null
			])
		]);
		$this->assertEmpty($fakeCall0);

		$fakeCall1 = $rbac->get($request);
		$this->assertNotEmpty($fakeCall1);

		$fakeCall2 = $rbac->get($request, 'fields');
		$this->assertNotEmpty($fakeCall2);

		$fakeCall3 = $rbac->get($request, 'resources');
		$this->assertNotEmpty($fakeCall3);

		$fakeCall4 = $rbac->get($request, 'conditions');
		$this->assertNotEmpty($fakeCall4);
	}

	public function testCanAccess_fail (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => [
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'rbac' => (object)[
					'fields' => (object)[
						'operator' => 'allow',
						'list' => (object)[
							'first' => 'firstName',
							'username' => 'username',
							'account' => 'account',
							'config' => 'config.M360.common.pet'
						]
					],
					'resources' => (object)[
						'mode' => 'own',
						'field' => 'account',
						'value' => 'account._id'
					],
					'conditions' => (object)[
						"operator" => '$and',
						"criteria" => [
							(object)[
								'function' => "EQ",
								"arguments" => (object)[
									"field" => "username",
									"value" => "username",
									"custom" => ""
								]
							],
							(object)[
								'function' => "EQ",
								"arguments" => (object)[
									"field" => "account",
									"value" => "account._id",
									"custom" => ""
								]
							],
							(object)[
								'function' => 'EQ',
								"arguments" => (object)[
									"field" => 'username',
									"value" => 'custom',
									"custom" => 'owner'
								]
							],
							(object)[
								'function' => 'EQ',
								"arguments" => (object)[
									"field" => 'config.M360.common.primary',
									"value" => 'custom',
									"custom" => 'true'
								]
							]
						]
					],
				]
			])
		];

		try {
			$fakeCall1 = $rbac->canAccess($request, 'invalid', (object)[]);
		} catch (\Exception $e) {
			$this->assertNotEmpty($e->getMessage());
		}
	}

	public function testCanAccess_fields_fail (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'fields' => (object)[
						'operator' => 'allow',
						'list' => (object)[
							'first' => 'firstName',
							'username' => 'username',
							'account' => 'account',
							'config' => 'config.M360.common.pet'
						]
					]
				]
			])
		];

		$fakeCall1 = $rbac->canAccess($request, 'fields', (object)[
			'config' => [
				'MIKE' => [
					'common' => [
						'pet' => 'cersei'
					]
				]
			]
		]);
		$this->assertEquals($fakeCall1, true);
	}

	public function testCanAccess_fields_RBAC_not_configured (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'fields' => (object)[]
				]
			])
		];

		$fakeCall1 = $rbac->canAccess($request, 'fields', (object)[
			'config' => [
				'MIKE' => [
					'common' => [
						'pet' => 'cersei'
					]
				]
			]
		]);

		$this->assertEquals($fakeCall1, true);
	}

	public function testCanAccess_fields_fail_invalidData (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'fields' => (object)[
						'operator' => 'allow',
						'list' => (object)[
							'first' => 'firstName',
							'username' => 'username',
							'account' => 'account',
							'config' => 'config.M360.common.pet'
						]
					]
				]
			])
		];

		try {
			$fakeCall1 = $rbac->canAccess($request, 'fields', [(object)[
				'config' => [
					'MIKE' => [
						'common' => [
							'pet' => 'cersei'
						]
					]
				]
			]]);
		} catch (\Exception $e) {
			$this->assertNotNull($e);
		}
	}

	public function testCanAccess_fields_fail_empty_contract (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => []
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'fields' => (object)[
						'operator' => 'allow',
						'list' => (object)[
							'first' => 'firstName',
							'username' => 'username',
							'account' => 'account',
							'config' => 'config.M360.common.pet'
						]
					]
				]
			])
		];

		$fakeCall1 = $rbac->canAccess($request, 'fields', (object)[
			'config' => [
				'MIKE' => [
					'common' => [
						'pet' => 'cersei'
					]
				]
			]
		]);
		$this->assertEquals($fakeCall1, true);
	}

	public function testCanAccess_fields_fail_deny (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["username", "config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'fields' => (object)[
						'operator' => 'deny',
						'list' => (object)[
							'first' => 'firstName',
							'username' => 'username',
							'account' => 'account',
							'config' => 'config.M360.common.pet'
						]
					]
				]
			])
		];

		$fakeCall1 = $rbac->canAccess($request, 'fields', (object)[
			"username" => "username",
			'config' => [
				'MIKE' => [
					'common' => [
						'pet' => 'cersei'
					]
				]
			]
		]);
		$this->assertEquals($fakeCall1, false);
	}

	public function testCanAccess_resources_invalid_data_fail (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'resources' => (object)[
						'mode' => 'any',
						'field' => 'account.id',
						'value' => 'account'
					]
				]
			])
		];

		try {
			$fakeCall1 = $rbac->canAccess($request, 'resources', [(object)[
				'account' => 'mike'
			]]);
		} catch (\Exception $e) {
			$this->assertNotNull($e);
		}
	}

	public function testCanAccess_resources_rbac_not_configured1_fail (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'resources' => (object)[
					]
				]
			])
		];

		$fakeCall1 = $rbac->canAccess($request, 'resources', (object)[
			'account' => 'mike'
		]);

		$this->assertEquals($fakeCall1, true);
	}

	public function testCanAccess_resources_rbac_not_configured2_fail (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'resources' => (object)[
						'mode' => 'own',
						'field' => 'account.id',
						'value' => ''
					]
				]
			])
		];

		$fakeCall1 = $rbac->canAccess($request, 'resources', (object)[
			'account' => 'mike'
		]);

		$this->assertEquals($fakeCall1, true);
	}

	public function testCanAccess_resources_rbac_configured_any_success (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'resources' => (object)[
						'mode' => 'any',
						'field' => 'account.id',
						'value' => ''
					]
				]
			])
		];

		$fakeCall1 = $rbac->canAccess($request, 'resources', (object)[
			'account' => 'mike'
		]);

		$this->assertEquals($fakeCall1, true);
	}

	public function testCanAccess_resources_rbac_empty_user_fail (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'resources' => (object)[
						'mode' => 'own',
						'field' => 'account.id',
						'value' => 'account'
					]
				]
			])
		];
		$userStub->method("get")->willReturn(null);
		$rbac->resetComponents($userStub);
		$fakeCall1 = $rbac->canAccess($request, 'resources', (object)[
			'account' => 'mike'
		]);
		$this->assertEquals($fakeCall1, false);
	}

	public function testCanAccess_resources_rbac_success (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'resources' => (object)[
						'mode' => 'own',
						'field' => 'account.id',
						'value' => 'account.id'
					]
				]
			])
		];
		$userStub->method("get")->willReturn((object)[
			'username' => 'mike',
			'account' => [
				'id' => 'mike'
			]
		]);

		$rbac->resetComponents($userStub);
		$fakeCall1 = $rbac->canAccess($request, 'resources', (object)[
			'account' => [
				'id' => 'mike'
			]
		]);
		$this->assertEquals($fakeCall1, true);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'resources' => (object)[
						'mode' => 'own',
						'field' => 'username',
						'value' => 'username'
					]
				]
			])
		];
		$fakeCall2 = $rbac->canAccess($request, 'resources', (object)[
			'username' => 'mike'
		]);
		$this->assertEquals($fakeCall2, true);

		$fakeCall2 = $rbac->canAccess($request, 'resources', (object)[
			'username' => 'hajj'
		]);
		$this->assertEquals($fakeCall2, false);
	}

	public function testCanAccess_conditions_invalid_data_or_not_configured_fail (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub = $this->createStub(M360\User::class);
		$tenantStub = $this->createStub(M360\Tenant::class);
		$utilsStub = $this->createStub(M360\Utils::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request1 = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'conditions' => (object)[
						'operator' => '$and',
						'criteria' => []
					]
				]
			])
		];

		$request2 = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'conditions' => (object)[
						'operator' => '$and',
						'criteria' => []
					]
				]
			])
		];

		try {
			$fakeCall1 = $rbac->canAccess($request1, 'conditions', [(object)[
				'account' => 'mike'
			]]);
		} catch (\Exception $e) {
			$this->assertNotNull($e);
		}

		$fakeCall2 = $rbac->canAccess($request2, 'conditions', (object)[
			'account' => 'mike'
		]);
		$this->assertEquals($fakeCall2, true);
	}

	public function testCanAccess_conditions_success (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$serviceStub = $this->createStub(M360\Service::class);
		$userStub1 = $this->createStub(M360\User::class);
		$userStub2 = $this->createStub(M360\User::class);
		$tenantStub1 = $this->createStub(M360\Tenant::class);
		$tenantStub2 = $this->createStub(M360\Tenant::class);

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request1 = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'conditions' => (object)[
						'operator' => '$and',
						'criteria' => [
							(object)[
								'function' => 'BAD',
								'field' => '',
								'value' => ''
							]
						]
					]
				]
			])
		];
		$userStub1->method("get")->willReturn(null);
		$rbac->resetComponents($userStub1);
		$fakeCall1 = $rbac->canAccess($request1, 'conditions', (object)[
			'account' => 'mike'
		]);
		$this->assertEquals($fakeCall1, false);

		$request2 = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'conditions' => (object)[
						'operator' => '$and',
						'criteria' => [
							(object)[
								'function' => 'BAD',
								'field' => '',
								'value' => ''
							]
						]
					]
				]
			])
		];
		$userStub2->method("get")->willReturn((object)[
			'username' => 'mike',
			'account' => [
				'id' => 'mike'
			]
		]);
		$tenantStub1->method("get")->willReturn((object)[
			'config' => [
				'test' => 'jj'
			]
		]);
		$rbac->resetComponents($userStub2, $tenantStub1);
		$fakeCall2 = $rbac->canAccess($request2, 'conditions', (object)[
			'account' => 'mike'
		]);
		$this->assertEquals($fakeCall2, false);


		$request3 = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'conditions' => (object)[
						'operator' => '$and',
						'criteria' => [
							(object)[
								"function" => "EMPTY",
								"arguments" => (object)[
									"field" => "password",
									"value" => "",
									"custom" => ""
								]
							],
							(object)[
								"function" => "NOT_EMPTY",
								"arguments" => (object)[
									"field" => "username",
									"value" => "",
									"custom" => ""
								]
							],
							(object)[
								'function' => 'EQ',
								'arguments' => (object)[
									'field' => 'account',
									'value' => 'account.id'
								]
							],
							(object)[
								"function" => "EQ",
								"arguments" => (object)[
									"field" => "primary",
									"value" => "custom",
									"custom" => "true"
								]
							],
							(object)[
								"function" => 'EQ',
								"arguments" => (object)[
									"field" => 'username',
									"value" => 'custom',
									"custom" => 'mike'
								]
							],
							(object)[
								"function" => 'NOT_EQ',
								"arguments" => (object)[
									"field" => 'username',
									"value" => 'custom',
									"custom" => 'owner'
								]
							],
							(object)[
								"function" => 'START',
								"arguments" => (object)[
									"field" => 'config.test',
									"value" => 'custom',
									"custom" => 'mi'
								]
							],
							(object)[
								"function" => 'NOT_START',
								"arguments" => (object)[
									"field" => 'config.test',
									"value" => 'custom',
									"custom" => 'jj'
								]
							],
							(object)[
								"function" => 'END',
								"arguments" => (object)[
									"field" => 'config.test',
									"value" => 'custom',
									"custom" => 'ke'
								]
							],
							(object)[
								"function" => 'NOT_END',
								"arguments" => (object)[
									"field" => 'config.test',
									"value" => 'custom',
									"custom" => 'jj'
								]
							],
							(object)[
								"function" => 'CONTAIN',
								"arguments" => (object)[
									"field" => 'config.test',
									"value" => 'custom',
									"custom" => 'ik'
								]
							],
							(object)[
								"function" => 'NOT_CONTAIN',
								"arguments" => (object)[
									"field" => 'config.test',
									"value" => 'custom',
									"custom" => 'jj'
								]
							],
							(object)[
								"function" => 'IN',
								"arguments" => (object)[
									"field" => 'config.test',
									"value" => 'custom',
									"custom" => 'ik'
								]
							],
							(object)[
								"function" => 'NOT_IN',
								"arguments" => (object)[
									"field" => 'config.test',
									"value" => 'custom',
									"custom" => 'jj'
								]
							],
							(object)[
								"function" => 'EQ',
								"arguments" => (object)[
									"field" => 'config.test',
									"value" => 'custom',
									"custom" => 'jj',
									"tCode" => 'TEST'
								]
							]
						]
					]
				]
			])
		];
		$fakeCall2 = $rbac->canAccess($request3, 'conditions', (object)[
			"id" => '12345',
			'username' => 'mike',
			'account' => 'mike',
			'password' => NULL,
			'email' => 'mike@corsairm360.com',
			'primary' => true,
			'config' => [
				'test' => 'mike'
			]
		]);
		$this->assertEquals($fakeCall2, true);


		$request4 = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'conditions' => (object)[
						'operator' => '$or',
						'criteria' => [
							(object)[
								"function" => "EMPTY",
								"arguments" => (object)[
									"field" => "password",
									"value" => "",
									"custom" => ""
								]
							],
							(object)[
								"function" => 'EQ',
								"arguments" => (object)[
									"field" => 'config.test',
									"value" => 'custom',
									"custom" => 'jj',
									"tCode" => 'TEST'
								]
							]
						]
					]
				]
			])
		];
		$tenantStub2->method("get")->willReturn(NULL);
		$rbac->resetComponents($userStub2, $tenantStub2);
		$fakeCall2 = $rbac->canAccess($request4, 'conditions', (object)[
			"id" => '12345',
			'username' => 'mike',
			'account' => 'mike',
			'password' => NULL,
			'email' => 'mike@corsairm360.com',
			'primary' => true,
			'config' => [
				'test' => 'mike'
			]
		]);
		$this->assertEquals($fakeCall2, true);
	}

	public function testfilterFields_invalidData (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["password", "config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request1 = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'fields' => (object)[
						'operator' => 'allow',
						'list' => (object)[
						]
					]
				]
			])
		];

		$data = (object)[
			"id" => '12345',
			'username' => 'mike',
			'account' => 'mike',
			'password' => 'hajj',
			'email' => 'mike@corsairm360.com',
			'primary' => true,
			'config' => [
				'test' => 'mike'
			]
		];
		$fakeCall1 = $rbac->filterFields($request1, $data);
		$this->assertEquals($fakeCall1, $data);

		$request2 = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'fields' => (object)[
						'operator' => 'allow',
						'list' => (object)[
							'username' => 'username',
							'password' => 'password',
							'account' => 'account',
							'config' => 'config.test'
						]
					]
				]
			])
		];

		$fakeCall1 = $rbac->filterFields($request2, [$data]);
		$this->assertEquals($fakeCall1, [
			[
				"id" => '12345',
				"username" => "mike",
				"account" => "mike",
				'password' => 'hajj',
				'email' => 'mike@corsairm360.com',
				'primary' => true,
				'config' => [
					'test' => 'mike'
				]
			]
		]);

		$request3 = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'fields' => (object)[
						'operator' => 'deny',
						'list' => (object)[
							'username' => 'username',
							'password' => 'password',
							'account' => 'account',
							'config' => 'config.test'
						]
					]
				]
			])
		];

		$fakeCall1 = $rbac->filterFields($request3, $data);
		$this->assertEquals($fakeCall1, (object)[
			"id" => '12345',
			'username' => 'mike',
			'account' => 'mike',
			'email' => 'mike@corsairm360.com',
			'primary' => true,
			"config" => []
		]);
	}

	public function testfilterFields_validData_with_list (): void {
		$configuration = (object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/" => (object)[
								"label" => "Test API",
								"access" => false,
								"rbac" => (object)[
									"fields" => ["password", "config"]
								]
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		];

		$rbac = new RBAC();
		$rbac->setServiceConfig($configuration);

		$request = [
			'm360' => json_encode((object)[
				'API' => (object)[
					'method' => 'get',
					'endpoint' => '/'
				],
				'rbac' => (object)[
					'fields' => (object)[
						'operator' => 'allow',
						'list' => (object)[
							'username' => 'username',
							'password' => 'password',
							'account' => 'account',
							'config' => 'config.list[0].foo'
						]
					]
				]
			])
		];

		$data = (object)[
			"id" => '12345',
			'username' => 'mike',
			'account' => 'mike',
			'password' => 'hajj',
			'email' => 'mike@corsairm360.com',
			'primary' => true,
			'config' => [
				'test' => 'mike',
				'list' => [
					'foo' => 'bar',
					'foo2' => 'bar2',
				]
			]
		];

		$fakeCall1 = $rbac->filterFields($request, $data);
		$this->assertEquals($fakeCall1, [
			"id" => '12345',
			'username' => 'mike',
			'account' => 'mike',
			'email' => 'mike@corsairm360.com',
			'primary' => true,
			'password' => 'hajj',
			"config" => [
				'foo' => 'bar'
			]
		]);
	}
}