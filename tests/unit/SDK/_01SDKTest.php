<?php declare(strict_types=1);

use M360\SDK;
use PHPUnit\Framework\TestCase;

/**
 * @covers M360\SDK
 * @covers M360\Schema
 * @covers M360\Logger
 */
final class _01SDKTest extends TestCase
{
	public function testInitInvalidConfiguration1 (): void {
		$sdk = new SDK();
		try {
			$sdk->init([
				'platform' => 'manual',
				'contract' => 'invalid',
				'ip' => "127.0.0.1",
				'type' => 'symfony'
			]);
		} catch (\Exception $e) {
			$this->assertEquals("The configuration provided is not well formatted! Please make sure you're using an Object instead of an Array", $e->getMessage());
		}
	}

	public function testInitInvalidConfiguration2 (): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'manual',
				'contract' => '',
				'ip' => "127.0.0.1",
				'type' => 'symfony'
			]);
		} catch (\Exception $e) {
			$this->assertNotEmpty($e->getMessage());
		}
	}

	public function testInitInvalidConfiguration3 (): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'manual',
				'contract' => [
					'name' => 'tester'
				],
				'ip' => "127.0.0.1",
				'type' => 'symfony'
			]);
		} catch (\Exception $e) {
			$this->assertNotEmpty($e->getMessage());
		}
	}

	public function testInitInvalidConfiguration4 (): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'manual',
				'contract' => "{'mike':'hajj'}",
				'ip' => "127.0.0.1",
				'type' => 'symfony'
			]);
		} catch (\Exception $e) {
			$this->assertNotEmpty($e->getMessage());
		}
	}

	public function testInitInvalidConfigurationwithReservedRoutes (): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'manual',
				'contract' => (object)[
					"name" => "dummyserver",
					"group" => "testing",
					"version" => 1,
					"ports" => (object)[
						"data" => 7001,
						"maintenance" => 7001
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/heartbeat" => (object)[
									"access" => false,
									"label" => "healthcheck"
								]
							]
						]
					]
				],
				'ip' => "127.0.0.1",
				'type' => 'symfony'
			]);
		} catch (\Exception $e) {
			$this->assertEquals("Route [GET] '{ /heartbeat }' is reserved by the middleware, please use a different route or method for your endpoint. ", $e->getMessage());
		}
	}

	public function testInitInvalidConfigurationwithWrongContractAPI (): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'manual',
				'contract' => (object)[
					"name" => "dummyserver",
					"group" => "testing",
					"version" => 1,
					"ports" => (object)[
						"data" => 7001,
						"maintenance" => 7001
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/test" => (object)[
									"foo" => "bar"
								]
							]
						]
					]
				],
				'ip' => "127.0.0.1",
				'type' => 'symfony'
			]);
		} catch (\Exception $e) {
			$this->assertEquals("Error in contract.apis.main.get./test.access => The property access is required
Error in contract.apis.main.get./test.label => The property label is required
Error in contract.apis.main.get./test => The property foo is not defined and the definition does not allow additional properties", $e->getMessage());
		}
	}

	public function testInitContainerInvalidConfiugration(): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'docker',
				'contract' => (object)[
					"name" => "dummyserver",
					"group" => "testing",
					"version" => 1,
					"ports" => (object)[
						"data" => 7001,
						"maintenance" => 7001
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/test" => (object)[
									"label" => "Test API",
									"access" => false
								]
							]
						]
					]
				],
				'ip' => "127.0.0.1",
				'type' => 'symfony'
			]);
		} catch (\Exception $e) {
			$this->assertEquals("Missing \"platformOptions\" for platform 'docker'. Please refer to the documentation for additional help!", $e->getMessage());
		}
	}

	public function testInitDockerContainerInvalidConfiugrationNoNetwork(): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'docker',
				'contract' => (object)[
					"name" => "dummyserver",
					"group" => "testing",
					"version" => 1,
					"ports" => (object)[
						"data" => 7001,
						"maintenance" => 7001
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/test" => (object)[
									"label" => "Test API",
									"access" => false
								]
							]
						]
					]
				],
				'ip' => "127.0.0.1",
				'type' => 'symfony',
				"platformOptions" => [
					"service" => "mike"
				]
			]);
		} catch (\Exception $e) {
			$this->assertEquals("Missing docker \"network\" name in platform options.", $e->getMessage());
		}
	}

	public function testInitDockerContainerInvalidConfiugrationNoService(): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'docker',
				'contract' => (object)[
					"name" => "dummyserver",
					"group" => "testing",
					"version" => 1,
					"ports" => (object)[
						"data" => 7001,
						"maintenance" => 7001
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/test" => (object)[
									"label" => "Test API",
									"access" => false
								]
							]
						]
					]
				],
				'ip' => "127.0.0.1",
				'type' => 'symfony',
				"platformOptions" => [
					"network" => "mike"
				]
			]);
		} catch (\Exception $e) {
			$this->assertEquals("Missing docker \"service\" name in platform options.", $e->getMessage());
		}
	}

	public function testInitK8sContainerInvalidConfiugrationNoNamespace(): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'kubernetes',
				'contract' => (object)[
					"name" => "dummyserver",
					"group" => "testing",
					"version" => 1,
					"ports" => (object)[
						"data" => 7001,
						"maintenance" => 7001
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/test" => (object)[
									"label" => "Test API",
									"access" => false
								]
							]
						]
					]
				],
				'ip' => "127.0.0.1",
				'type' => 'symfony',
				"platformOptions" => [
					"service" => "mike",
					"exposedPort" => 80
				]
			]);
		} catch (\Exception $e) {
			$this->assertEquals("Missing kubernetes \"namespace\" value in platform options.", $e->getMessage());
		}
	}

	public function testInitK8sContainerInvalidConfiugrationNoService(): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'kubernetes',
				'contract' => (object)[
					"name" => "dummyserver",
					"group" => "testing",
					"version" => 1,
					"ports" => (object)[
						"data" => 7001,
						"maintenance" => 7001
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/test" => (object)[
									"label" => "Test API",
									"access" => false
								]
							]
						]
					]
				],
				'ip' => "127.0.0.1",
				'type' => 'symfony',
				"platformOptions" => [
					"namespace" => "mike",
					"exposedPort" => 80
				]
			]);
		} catch (\Exception $e) {
			$this->assertEquals("Missing kubernetes \"service\" name in platform options.", $e->getMessage());
		}
	}

	public function testInitK8sContainerInvalidConfiugrationNoPorts(): void {
		$sdk = new SDK();
		try {
			$sdk->init((object)[
				'platform' => 'kubernetes',
				'contract' => (object)[
					"name" => "dummyserver",
					"group" => "testing",
					"version" => 1,
					"ports" => (object)[
						"data" => 7001,
						"maintenance" => 7001
					],
					"apis" => (object)[
						"main" => (object)[
							"get" => (object)[
								"/test" => (object)[
									"label" => "Test API",
									"access" => false
								]
							]
						]
					]
				],
				'ip' => "127.0.0.1",
				'type' => 'symfony',
				"platformOptions" => [
					"namespace" => "mike",
					"service" => "mike"
				]
			]);
		} catch (\Exception $e) {
			$this->assertEquals("Missing kubernetes \"exposedPort\" value for container in platform options.", $e->getMessage());
		}
	}

	public function testInitDockerValidConfiguration(): void {
		$sdk = new SDK();
		$response = $sdk->init((object)[
			'platform' => 'docker',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony',
			"platformOptions" => [
				"network" => "mike",
				"service" => "mike"
			]
		]);

		$this->assertEquals($response, null);
	}

	public function testAutoRegisterValidServiceRegistration(): void {

		$stub1 = $this->createStub(\M360\Registry::class);
		$stub1->method('get')->willReturn((object)[
			'_TTL' => 500
		]);

		$stub2 = $this->createStub(\M360\Service::class);
		$stub2->method('register')->willReturn((object)[
			'notifications'=> [
				'service has been registerd ...'
			]
		]);

		$sdk = new SDK();
		$response = $sdk->init((object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		], $stub1, null, $stub2);

		$this->assertEquals($response, null);
		$sdk->autoRegister();
	}

	public function testAutoRegisterValidServiceRegistrationEmptyRegistry(): void {

		$stub1 = $this->createStub(\M360\Registry::class);
		$stub1->method('get')->willReturn(null);

		$stub2 = $this->createStub(\M360\Service::class);
		$stub2->method('register')->willReturn((object)[
			'notifications'=> [
				'service has been registerd ...'
			]
		]);

		$sdk = new SDK();
		$response = $sdk->init((object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'symfony'
		], $stub1, null, $stub2);

		$this->assertEquals($response, null);
		$sdk->autoRegister();

	}

	public function testInitJobValidConfiguration(): void {
		$stub1 = $this->getMockBuilder(\M360\Registry::class)->getMock();
		$stub1->method('get')->willReturn((object)['_TTL' => 500 ]);

		$stub2 = $this->createStub(\M360\Awareness::class);
		$stub2->method('init')->willReturn(null);

		$stub3 = $this->createStub(\M360\Service::class);
		$stub3->method('get')->willReturn((object)[]);

		$sdk = new SDK();
		$response = $sdk->init((object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'job'
		], $stub1, $stub2, $stub3);

		$this->assertEquals($response, true);
	}

	public function testInitJobValidConfigurationEmptyRegistry(): void {
		$stub1 = $this->getMockBuilder(\M360\Registry::class)->getMock();
		$stub1->method('get')->willReturn(null);

		$stub2 = $this->createStub(\M360\Awareness::class);
		$stub2->method('init')->willReturn(null);

		$stub3 = $this->createStub(\M360\Service::class);
		$stub3->method('get')->willReturn((object)[]);

		$sdk = new SDK();
		$response = $sdk->init((object)[
			'platform' => 'manual',
			'contract' => (object)[
				"name" => "dummyserver",
				"group" => "testing",
				"version" => 1,
				"ports" => (object)[
					"data" => 7001,
					"maintenance" => 7001
				],
				"apis" => (object)[
					"main" => (object)[
						"get" => (object)[
							"/test" => (object)[
								"label" => "Test API",
								"access" => false
							]
						]
					]
				]
			],
			'ip' => "127.0.0.1",
			'type' => 'job'
		], $stub1, $stub2, $stub3);

		$this->assertEquals($response, true);
	}
}