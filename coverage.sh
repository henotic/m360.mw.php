#!/bin/bash

export XDEBUG_MODE=coverage
export USE_ZEND_ALLOC=0
export MW_TESTING=true

echo "Running M360 Unit Tests ..."
#./vendor/bin/phpunit --generate-configuration
./vendor/bin/phpunit tests --coverage-html tests/coverage --coverage-filter M360/