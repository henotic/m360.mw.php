<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360;

use Curl;

class Service
{

	private $curl;
	private $configuration;
	private $GATEWAY_MAINTENANCE_IP;
	private $GATEWAY_MAINTENANCE_PORT;
	private $GATEWAY_MAINTENANCE_SSL;

	public function __construct () {

	}

	public function init ($configuration, Curl\Curl $curl = null) {
		$this->configuration = $configuration;
		if (!empty($this->configuration->gatewayMaintenanceIP)) {
			$this->GATEWAY_MAINTENANCE_IP = $this->configuration->gatewayMaintenanceIP;
		} else {
			$this->GATEWAY_MAINTENANCE_IP = isset($_SERVER["GATEWAY_MAINTENANCE_IP"]) ? $_SERVER["GATEWAY_MAINTENANCE_IP"] : '127.0.0.1';
		}

		if (!empty($this->configuration->gatewayMaintenancePort)) {
			$this->GATEWAY_MAINTENANCE_PORT = $this->configuration->gatewayMaintenancePort;
		} else {
			$this->GATEWAY_MAINTENANCE_PORT = isset($_SERVER["GATEWAY_MAINTENANCE_PORT"]) ? $_SERVER["GATEWAY_MAINTENANCE_PORT"] : 5000;
		}

        if (!empty($this->configuration->gatewayMaintenanceSSL)) {
            $this->GATEWAY_MAINTENANCE_SSL = $this->configuration->gatewayMaintenanceSSL;
        } else {
            $this->GATEWAY_MAINTENANCE_SSL = isset($_SERVER["GATEWAY_MAINTENANCE_SSL"]) ? $_SERVER["GATEWAY_MAINTENANCE_SSL"] : false;
        }

		if (!empty($curl)) {
			$this->curl = $curl;
		} else {
			$this->curl = new Curl\Curl();
		}

		return null;
	}

	/**
	 * Ensure the IP address value in the configuration object provided based on the platform being used.
	 * @return null
	 * @throws \Exception
	 */
	public function ensureIPAddress ($dns_get_record = null) {

		if (in_array(strtolower($this->configuration->platform), ["kubernetes"])) {
			return null;
		} else {
			$method = (!empty($dns_get_record)) ? $dns_get_record : 'dns_get_record';
			try {
				$hosts = $method($this->configuration->ip, DNS_ALL);
				foreach ($hosts as $host) {
					if ($host['class'] == 'IN' && $host['type'] == 'A') {
						$this->configuration->ip = $host["ip"];
					}
				}
			} catch (\Exception $e) {
				echo "\n" . $e->getMessage();
			}

			return null;
		}
	}

	/**
	 * This method formulates a payload from provided parameters and make a POST request call to the gateway maintenance API to register the service.
	 * @return type
	 * @throws \Exception
	 */
	public function register () {
		if (empty($this->configuration->contract) || gettype($this->configuration->contract) != 'object') {
			throw new \Exception("Missing/Invalid Service Contract!");
		}

		if ($this->configuration->ip == "false") {
			throw new \Exception("Missing/Invalid IP Address Value!");
		}

		$payload = $this->configuration->contract;

		if ($this->configuration && $this->configuration->platform == 'kubernetes') {
			$payload->ports->data = $this->configuration->platformOptions['exposedPort'];
			$payload->ports->maintenance = $this->configuration->platformOptions['exposedPort'];
		}

		$payload->host = (object)[
            "ssl" => (isset($this->configuration->contract->host) && isset($this->configuration->contract->ssl)) ? $this->configuration->contract->ssl : false,
			"weight" => (isset($this->configuration->contract->host) && isset($this->configuration->contract->host->weight)) ? intval($this->configuration->contract->host->weight) : 1,
			"hostname" => $this->configuration->ip,
			"ip" => $this->configuration->ip
		];

		$counter = 1;
		$max = 3;
		$delay = isset($_SERVER["MW_TESTING"]) ? 1 : 5000;

		$payload = json_decode(json_encode($payload), true);
		return $this->_makeRegisterRequest($payload, $counter, $max, $delay);
	}

	/**
	 * Will make request call and work with the "circuit breaker"
	 * @param type $payload
	 * @param type $counter
	 * @param type $max
	 * @param type $delay
	 * @return type
	 * @throws \Exception
	 */
	private function _makeRegisterRequest ($payload = array(), $counter = 1, $max = 3, $delay) {

        $protocol = ($this->GATEWAY_MAINTENANCE_SSL) ? 'https' : 'http';
		$url = $protocol . "://" . $this->GATEWAY_MAINTENANCE_IP . ":" . $this->GATEWAY_MAINTENANCE_PORT . "/service/register";
		$curl = $this->curl;
		$curl->setHeader("Content-Type", "application/json");
		$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
		$curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);

		$curl->post($url, $payload, true);

		$body = json_decode($curl->getResponse(), false);
		$inTime = new timeConvert\Time($delay);
		$preetyTime = $inTime->milToTime();
		if ($curl->curl_error) {
			if ($counter < $max) {
				$counter++;
				echo("\nRequest (Register Service) failed. Retrying { $counter }/{ $max } in { $preetyTime } hours ...\n");
				sleep($inTime->milToSecs());
				$this->_makeRegisterRequest($payload, $counter, $max, $delay);
			} else {
				echo("\nFailed registering service after { $counter }/{ $max } attempts!");
				throw new \Exception($curl->error_message, $curl->error_code);
			}
		} elseif ($body && (!@$body->result || @$body->errors)) {
			if ($counter < $max) {
				$counter++;
				echo("\nRequest (get Service) failed. Retrying { $counter }/{ $max } in { $preetyTime } hours ...\n");
				sleep($inTime->milToSecs());
				$this->_makeRegisterRequest($payload, $counter, $max, $delay);
			} else {
				if (@$body->statusCode) {
					throw new \Exception($body->message, @$body->code);
				}
				$errors = '';
				foreach ($body->errors as $codes) {
					$errors .= "Error {$codes->code} : {$codes->message}\n";
				}
				echo("\nFailed getting service after { $counter }/{ $max } attempts!\n");
				throw new \Exception($errors);
			}
		} else {
			if (gettype($body->data) == 'string') {
				$body->data = json_decode($body->data, false);
			}
			return $body->data;
		}
	}

	/**
	 * Get the service contract
	 * @param type $contract
	 * @return type
	 * @throws \Exception
	 */
	public function get () {
		if (!isset($this->configuration->contract) || gettype($this->configuration->contract) !== 'object') {
			throw new \Exception("Missing/Invalid Service Contract!");
		}
		$payload = $this->configuration->contract;

		$counter = 1;
		$max = 3;
		$delay = isset($_SERVER["MW_TESTING"]) ? 1 : 5000;
		return $this->_makeGetRequest($payload, $counter, $max, $delay);
	}

	/**
	 * will call the request and return the body data
	 * @param type $payload
	 * @param type $counter
	 * @param type $max
	 * @param type $delay
	 * @return type
	 * @throws \Exception
	 */
	private function _makeGetRequest ($payload, $counter, $max, $delay) {

		$protocol = ($this->GATEWAY_MAINTENANCE_SSL) ? 'https' : 'http';
		$url = $protocol . "://" . $this->GATEWAY_MAINTENANCE_IP . ":" . $this->GATEWAY_MAINTENANCE_PORT . "/service";
		$headers = array(
			"service" => $this->configuration->contract->name,
			"version" => $this->configuration->contract->version,
		);
		$curl = $this->curl;
		foreach ($headers as $key => $value) {
			$curl->setHeader($key, $value);
		}

		$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
		$curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);

		$curl->get($url, array(), true);
		$body = json_decode($curl->getResponse(), false);

		$inTime = new timeConvert\Time($delay);
		$preetyTime = $inTime->milToTime();

		if ($curl->curl_error) {
			if ($counter < $max) {
				$counter++;
				echo("\nRequest (get Service) failed. Retrying { $counter }/{ $max } in { $preetyTime } hours ...\n");
				sleep($inTime->milToSecs());
				$this->_makeGetRequest($payload, $counter, $max, $delay);
			} else {
				echo("\nFailed getting service after { $counter }/{ $max } attempts!\n");
				throw new \Exception($curl->error_message, $curl->error_code);
			}
		} elseif ($body && (!@$body->result || @$body->errors)) {
			if ($counter < $max) {
				$counter++;
				echo("\nRequest (get Service) failed. Retrying { $counter }/{ $max } in { $preetyTime } hours ...\n");
				sleep($inTime->milToSecs());
				$this->_makeGetRequest($payload, $counter, $max, $delay);
			} else {
				if (@$body->statusCode) {
					throw new \Exception($body->message, @$body->code);
				}
				$errors = '';
				foreach ($body->errors as $codes) {
					$errors .= "Error {$codes->code} : {$codes->message}\n";
				}
				echo("\nFailed getting service after { $counter }/{ $max } attempts!\n");
				throw new \Exception($errors);
			}
		} else {
			if (gettype($body->data) == 'string') {
				$body->data = json_decode($body->data, false);
			}
			return $body->data;
		}
	}

	/**
	 * This method returns the request endpoint information that was detected by the gateway and injected in the headers
	 * @param {Object} $request
	 */
	public function getEndpointInfo ($request) {
		if (!empty($request) && array_key_exists("m360", $request)) {
			$m360 = json_decode($request['m360'], false);
			return isset($m360->API) ? $m360->API : null;
		}

		return null;
	}
}
