<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360;

class Awareness
{
	public $mycache;
	private $user;

	/**
	 * Construct will start with a TLL provided in seconds
	 * @param type $TTL
	 */
	public function __construct () {

	}

	public function init ($TTL = 30) {
		$this->TTL = $TTL;
		$this->mycache = new M360Cache($this->TTL);
		$this->user = new User();
	}

	/**
	 * resets the caching ttl value of the awareness
	 * @param $TTL
	 */
	public function resetTTL ($TTL) {
		$this->TTL = $TTL;
	}

	/**
	 * Will receive the gateway connector object
	 * @param type $gatewayConnector
	 */
	public function setGatewayConnector ($gatewayConnector) {
		$this->GatewayConnector = $gatewayConnector;
		$this->user->setGatewayConnector($gatewayConnector);
	}

	/**
	 * Override the default user module
	 * @param $userModule
	 */
	public function setUserModule($userModule){
		$this->user = $userModule;
	}

	/**
	 * This method reloads the cached awareness
	 * @param {string} ip
	 * @param {string|Number} port
	 * @param {Array} headers
	 * @param {Function} cb
	 */
	public function reload ($ip = null, $port = null, $headers = []) {
		$this->mycache->flush();
		return $this->get($ip, $port, $headers);
	}

	/**
	 * will check if we have memcache data for the awareness data
	 * if not, we will go ahead and use the gateway invoke to gather the info and store in cache
	 * @param {string} $ip
	 * @param {Number} $port
	 * @param {Array} headers
	 */
	public function get ($ip = null, $port = null, $headers = []) {
		$noCache = (!empty($headers) && $headers['Cache-Control'] === 'no-cache');
		if (!$noCache && $this->mycache->get("awareness_{$ip}_{$port}")) {
			return $this->mycache->get("awareness_{$ip}_{$port}");
		}

		$params = array(
			"ip" => $ip,
			"port" => $port,
			"route" => "/awareness",
			"decrypt" => true,
			"headers" => $headers,
            "ssl" => isset($headers['Secure'])
		);

		$response = $this->GatewayConnector->invoke($params);
		if (property_exists($response, "error")) {
			return "Error: {$response->error} => {$response->message}";
		}
		$this->mycache->delete("awareness_{$ip}_{$port}");
		$this->mycache->set("awareness_{$ip}_{$port}", $response, $this->TTL);
		return $response;
	}

	/**
	 * will call the next host available by invoke
	 * @param type $service
	 * @param type $version
	 * @param type $ip
	 * @param type $port
	 * @return type
	 */
	public function getNextHost ($service, $version, $ip = "", $port = "") {
		$params = array(
			"ip" => $ip,
			"port" => $port,
			"route" => '/service/nextHost',
			"decrypt" => true,
			"qs" => array(
				"service" => $service,
				"version" => $version
			)
		);
		return $this->GatewayConnector->invoke($params);
	}

	/**
	 * will call proxy to gather the info on behalf of
	 * @param {object} $context
	 */
	public function proxy ($context) {
		$response = $this->getNextHost($context->service, $context->version);

		if (property_exists($context, "user") && !empty($context->user)) {
			$user = $this->getUser($context->user);

			if(empty($user)){
				return "Error: {$response->error} => User Not Found!";
			}

			if(empty($context->headers) || !isset($context->headers)){
				$context->headers = [];
            }

			$context->headers['m360'] = $user->m360;

			if(isset($user->tenants) && !empty($user->tenants) && is_array($user->tenants)) {
				$context->headers['m360']->tenants = array();

				foreach ($user->tenants as $oneTenant) {
					$tConfig = $oneTenant->code;
					$tenant = [
						"id" => $oneTenant->id,
						"code" => $oneTenant->code,
						"config" => $this->getConfig($user->config->$tConfig, $context->service)
					];
					array_push($context->headers['m360']->tenants, $tenant);
				}
			}

			$context->headers['m360'] = json_encode($context->headers['m360'], JSON_UNESCAPED_UNICODE);
		}

		return $this->proxyCall($response, $context);
	}

	private function getConfig ($config, $microservice) {
        $finalConfig = [];
        if(!empty($config)){
            //check and merge common
            if (!empty($config->common) && is_object($config->common) ) {
            	foreach($config->common as $key => $value){
		            $finalConfig[$key] = $value;
	            }
            }

            //check and merge specific per microservice
            if (!empty($config->specific) && !empty($microservice) && property_exists($config->specific, $microservice)) {

                $tempConfig = (property_exists($config->specific->$microservice,'config')) ? $config->specific->$microservice->config : $config->specific->$microservice;
	            foreach($tempConfig as $key => $value){
	            	$finalConfig[$key] = $value;
	            }

                if(!empty($config->specific->$microservice->config) && !empty($config->specific->$microservice->version)){
                    $finalConfig['__M360SV_' + $microservice] = $config->specific->$microservice->version;
                }
            }
        }

        return (object)$finalConfig;
    }

	/**
	 * proxy call construction
	 * @param {string} $host
	 * @param {object} $context
	 */
	private function proxyCall ($host, $context) {
		$params = [
			"ip" => $host->ip,
			"port" => $host->port,
			"route" => $context->route,
			"encrypt" => !empty($context->user) && !empty($context->user->m360),
			"decrypt" => false,
			"qs" => property_exists($context, "qs") ? (array)$context->qs : array(),
			"body" => property_exists($context, "body") ? (array)$context->body : array(),
			"headers" => property_exists($context, "headers") ? (array)$context->headers : array(),
			"method" => $context->method
		];
		return $this->GatewayConnector->invoke($params);
	}

	/**
	 * will find the user by unique id
	 * @param {string} $userName
	 */
	private function getUser ($userName) {
		return $this->user->find(array("id" => $userName));
	}
}
