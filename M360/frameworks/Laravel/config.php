<?php

$contractLocation = $_SERVER['M360_SERVICE_CONTRACT'];
$appIP = !empty($_SERVER['APP_IP']) ? $_SERVER['APP_IP'] : "127.0.0.1";
$appPlatform = !empty($_SERVER['APP_PLATFORM']) ? $_SERVER['APP_PLATFORM'] : "manual";
$appPlatformOptions = [
	"service" => !empty($_SERVER['APP_PLATFORM_OPTIONS_SERVICE']) ? $_SERVER['APP_PLATFORM_OPTIONS_SERVICE'] : "",
	"namespace" => !empty($_SERVER['APP_PLATFORM_OPTIONS_NAMESPACE']) ? $_SERVER['APP_PLATFORM_OPTIONS_NAMESPACE'] : "",
	"exposedPort" => !empty($_SERVER['APP_PLATFORM_OPTIONS_PORTS']) ? $_SERVER['APP_PLATFORM_OPTIONS_PORTS'] : "",
	"network" => !empty($_SERVER['APP_PLATFORM_OPTIONS_NETWORK']) ? $_SERVER['APP_PLATFORM_OPTIONS_NETWORK'] : ""
];

$gatewayMaintenanceIP = !empty($_SERVER['GATEWAY_MAINTENANCE_IP']) ? $_SERVER['GATEWAY_MAINTENANCE_IP'] : "127.0.0.1";
$gatewayMaintenancePort = !empty($_SERVER['GATEWAY_MAINTENANCE_PORT']) ? $_SERVER['GATEWAY_MAINTENANCE_PORT'] : "5000";

return [
	//type of framework running
	'type' => 'laravel',

	//the location of the contract.json file that holds the service api contract information
	'contract' => file_get_contents($contractLocation),

	//A reachable IP address or URL that the gateway will use to communicate with this application
	//NOTE: if URL is provided, it must use HTTPS and do not add HTTPS to the value
	//Example: URL is https://www.myapp.com --> Provider: www.myapp.com (only)
	'ip' => $appIP,

	//The hosting platform type (manual / docker / kubernetes)
	'platform' => $appPlatform,

	//if platform is docker or kubernetes, provide the options
	'platformOptions' => $appPlatformOptions,

    'gatewayMaintenanceIP' => $gatewayMaintenanceIP,

    'gatewayMaintenancePort' => $gatewayMaintenancePort
];
