<?php

namespace M360\frameworks\Laravel;

use Closure;

class Middleware extends SP
{
	public function __construct () {
		parent::__construct();
		parent::init();
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle ($request, Closure $next) {
		$request->M360 = $this->M360Mw;
		return $next($request);
	}
}