<?php

namespace M360\frameworks\Laravel;

use Illuminate\Console\Command;


class RegistryReload extends Command
{
	protected $signature = 'm360:reloadregistry';
	protected $description = 'Reloads the Registry of the M360 middleware in this laravel server';

	private $M360mw;

	public function __construct () {
		parent::__construct();
		$this->M360mw = new SP();
	}

	public function handle () {
		$this->M360mw->init();
		print_r($this->M360mw->reloadRegistry());
	}
}