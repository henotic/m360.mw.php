<?php

namespace M360\frameworks\Laravel;

use Illuminate\Console\Command;


class AutoRegister extends Command
{
	protected $signature = 'm360:autoregister';
	protected $description = 'Auto Register this laravel server in M360 Gateway';

	private $M360mw;

	public function __construct () {
		parent::__construct();
		$this->M360mw = new SP();
	}

	public function handle () {
		//initialize the middleware
		$this->M360mw->init();

		//auto register
		$this->M360mw->autoRegister();
	}
}