<?php
/*
 * Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360\frameworks\Laravel;

use Exception;
use M360;
use M360\Maintenance\Maintenance;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

use Illuminate\Routing\Router;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Artisan;

class SP extends ServiceProvider
{
	protected $M360Mw;
	private $appName;
	private $maintenance;

	//create new instance of the middleware
	public function __construct () {
		$this->M360Mw = new \M360\SDK();
	}

	/**
	 * Initialize the middleware and pass on required inputs from config files
	 * @throws Exception
	 */
	public function init () {
		try {
            $_SERVER['GATEWAY_MAINTENANCE_IP'] = config('m360.gatewayMaintenanceIP');
            $_SERVER['GATEWAY_MAINTENANCE_PORT'] = config('m360.gatewayMaintenancePort');
			$this->M360Mw->init((object)[
				'type' => config('m360.type', 'laravel'),
				'contract' => config('m360.contract', file_get_contents(__DIR__ . '/sample_contract.json')),
				'ip' => config('m360.ip', "127.0.0.1"),
				'platform' => config('m360.platform', 'manual'),
				'platformOptions' => config('m360.platformOptions', []),
				'gatewayMaintenanceIP' => config('m360.gatewayMaintenanceIP'),
				'gatewayMaintenancePort' => config('m360.gatewayMaintenancePort')
			]);
		} catch (Exception $e) {
			throw $e;
		}
	}

	/**
	 * boot method (called second on every request), initialize the mw at request level
	 */
	public function boot (Router $router, Kernel $kernel, Schedule $schedule) {

		//register the commands in the artisan of laravel
		$this->commands([
			M360\frameworks\laravel\Install::class,
			M360\frameworks\laravel\AutoRegister::class,
			M360\frameworks\laravel\RegistryReload::class
		]);

		//initialize the middleware
		$this->init();

		//register M360 as a middleware
		$kernel->prependMiddleware(\M360\frameworks\Laravel\Middleware::class);
		$router->aliasMiddleware('M360', \M360\frameworks\Laravel\Middleware::class);

		//attach the routes
		$this->augment();
	}

	/**
	 * method that triggers the reload registry procedure
	 */
	public function reloadRegistry () {
		return $this->M360Mw->registry->reload();
	}

	/**
	 * method that triggers the auto register procedure
	 */
	public function autoRegister () {
		$this->M360Mw->autoRegister();
	}

	/**
	 * Sets the M360 custom modules that will be used by laravel
	 * augments laravel and adds new routes
	 */
	public function augment () {
		$this->appName = $this->M360Mw->configuration->contract->name;
		$this->maintenance = new Maintenance(
			$this->appName,
			$this->M360Mw->configuration,
			$this->M360Mw->registry,
			$this->M360Mw->service,
			$this->M360Mw->awareness
		);

		//add the extra routes
		$this->addRoutes();
	}

	/**
	 * add M360 routes to laravel
	 */
	private function addRoutes () {
		//auto hook the heartbeat maintenance api
		Route::get('/heartbeat', function (Request $request) {
			return response(json_encode([
				"name" => $this->appName,
				"heartbeat" => true
			], JSON_PRETTY_PRINT), 200)->header('Content-Type', 'text/plain');
		});

		//auto hook the reload registry maintenance api
		Route::get('/registry/reload', function (Request $request) {
			$registry = $this->maintenance->reloadRegistry($request);
			if (!empty($registry)) {
				return response(json_encode($registry, JSON_PRETTY_PRINT), 200)->header('Content-Type', 'application/json');
			} else {
				return response(json_encode(['error' => 'Error reloading the registry!']), 200)->header('Content-Type', 'application/json');
			}
		});

		//auto hook the reload $awareness maintenance api
		Route::get('/awareness/reload', function (Request $request) {
			$awareness = $this->maintenance->reloadAwareness($request);
			if (!empty($awareness)) {
				return response(json_encode($awareness, JSON_PRETTY_PRINT), 200)->header('Content-Type', 'application/json');
			} else {
				return response(json_encode(['error' => 'Error reloading the awareness!']), 200)->header('Content-Type', 'application/json');
			}
		});
	}
}
