<?php

namespace M360\frameworks\Laravel;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class Install extends Command
{
	protected $signature = 'm360:install';

	protected $description = 'Install the M360 Middleware';

	public function handle () {
		$this->info('Installing M360...');

		if (!$this->configExists('m360.php')) {
			$this->publishConfiguration();
			$this->info('Middleware Configuration created ...');
		} else {
			if ($this->shouldOverwriteConfig()) {
				$this->info('Overwriting configuration file...');
				$this->publishConfiguration($force = true);
			} else {
				$this->info('Existing configuration was not overwritten');
			}
		}

		$this->info('Middleware Installed');
	}

	private function configExists ($fileName) {
		return File::exists(config_path($fileName));
	}

	private function shouldOverwriteConfig () {
		return $this->confirm(
			'Config file already exists. Do you want to overwrite it?',
			false
		);
	}

	private function publishConfiguration () {
		File::copy(__DIR__ . "/config.php" ,"config/m360.php");

		echo "\n\n";
		echo "M360API PHP Middleware has been Installed!";
		echo "Please run the artisan command everytime your start/restart your server:";
		echo "This commands updates the M360API Gateway and provides it with a new copy of the server's API contract.";
		echo "";
		echo "> php artisan m360:autoregister";
		echo "";
		echo "Please add the following artisan command to your crontab:";
		echo "This commands enables he middleware to periodically updated its cached copy of the the M360API's registry.";
		echo "";
		echo "> php arisan m360:reloadregistry";
		echo "";
		echo "\n\n";
	}
}