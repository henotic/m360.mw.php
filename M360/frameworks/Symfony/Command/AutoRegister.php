<?php

namespace M360\frameworks\Symfony\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use M360\frameworks\Symfony\Service\M360MiddlewareService;

/**
 * Class AutoRegister
 * @package M360\frameworks\Symfony
 * @example php bin/console m360:autoregister
 */
class AutoRegister extends Command
{
	protected static $defaultName = 'm360:autoregister';
	protected static $defaultDescription = 'Auto Register this laravel server in M360 Gateway';

	private M360MiddlewareService $M360mw;

	public function __construct (M360MiddlewareService $m360MiddlewareService) {
		parent::__construct();
		$this->M360mw = $m360MiddlewareService;
	}

	public function execute (InputInterface $input, OutputInterface $output): int {
		//initialize the middleware
		try {
			$this->M360mw->init();
		} catch (\Exception $e) {
			$output->write($e->getMessage());
			return Command::FAILURE;
		}

		//auto register
		$this->M360mw->autoRegister();
		return Command::SUCCESS;
	}
}