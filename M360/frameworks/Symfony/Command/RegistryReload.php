<?php

namespace M360\frameworks\Symfony\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use M360\frameworks\Symfony\Service\M360MiddlewareService;

/**
 * Class RegistryReload
 * @package M360\frameworks\Symfony
 * @example php bin/console m360:reloadregistry
 */
class RegistryReload extends Command
{
	protected static $defaultName = 'm360:reloadregistry';
	protected static $defaultDescription = 'Reloads the Registry of the M360 middleware in this laravel server';

	private M360MiddlewareService $M360mw;

	public function __construct (M360MiddlewareService $m360MiddlewareService) {
		parent::__construct();
		$this->M360mw = $m360MiddlewareService;
	}

	public function execute (InputInterface $input, OutputInterface $output): int {
		try {
			$this->M360mw->init();
		} catch (\Exception $e) {
			$output->write($e->getMessage());
			return Command::FAILURE;
		}
		$output->write(json_encode($this->M360mw->reloadRegistry(), JSON_PRETTY_PRINT));
		return Command::SUCCESS;
	}
}