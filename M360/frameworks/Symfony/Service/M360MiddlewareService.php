<?php

namespace M360\frameworks\Symfony\Service;

use Exception;
use M360;
use M360\Maintenance\Maintenance;

class M360MiddlewareService
{

	public $SDK;
	protected $appName;
	protected $maintenance;
	private $config;

	//create new instance of the middleware
	public function __construct ($m360) {
		$this->SDK = new \M360\SDK();
		$this->config = $m360;
	}

	/**
	 * Return the App Name
	 */
	public function getAppName(){
		return $this->appName;
	}

	/**
	 * Return the App Maintenance SDK
	 */
	public function getAppMaintenance(){
		return $this->maintenance;
	}

	/**
	 * Initialize the middleware and pass on required inputs from config files
	 * @throws Exception
	 */
	public function init () {
		try {
			$this->SDK->init((object)[
				'type' => $this->config['type'],
				'contract' => file_get_contents($this->config['contract']),
				'ip' => $this->config['ip'],
				'platform' => $this->config['platform'],
				'platformOptions' => !empty($this->config['platformOptions']) ? $this->config['platformOptions'] : []
			]);
		} catch (Exception $e) {
			throw $e;
		}

		//attach the routes
		$this->augment();
	}

	/**
	 * method that triggers the reload registry procedure
	 */
	public function reloadRegistry () {
		return $this->SDK->registry->reload();
	}

	/**
	 * method that triggers the auto register procedure
	 */
	public function autoRegister () {
		$this->SDK->autoRegister();
	}

	/**
	 * Sets the M360 custom modules that will be used by laravel
	 * augments laravel and adds new routes
	 */
	public function augment () {
		$this->appName = $this->SDK->configuration->contract->name;
		$this->maintenance = new Maintenance(
			$this->appName,
			$this->SDK->configuration,
			$this->SDK->registry,
			$this->SDK->service,
			$this->SDK->awareness
		);

	}
}