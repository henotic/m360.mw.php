<?php

namespace M360\frameworks\Symfony\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

use Symfony\Component\Filesystem\Filesystem;

/**
 * M360Extension
 */
class M360MiddlewareExtension extends Extension
{
	public function load (array $configs, ContainerBuilder $container) {

		$loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../config'));
		$loader->load('services.yaml');

		$configuration = new Configuration();
		$this->processConfiguration($configuration, [$container->getParameter('m360')]);

		$filesystem = new Filesystem();
		$destinationFile = $container->getParameter('kernel.project_dir') . '/config/routes/m360.yaml';
		$sourceFile = __DIR__ . '/../config/m360.yaml';

		if(!$filesystem->exists($destinationFile)){
			$filesystem->copy($sourceFile, $destinationFile);
		}
	}
}