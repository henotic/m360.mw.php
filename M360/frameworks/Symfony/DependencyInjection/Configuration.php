<?php

namespace M360\frameworks\Symfony\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

	public function getConfigTreeBuilder () {

		$treeBuilder = new TreeBuilder('m360');

		//type of framework running
		$treeBuilder->getRootNode()
			->children()
					->scalarNode('type')->defaultValue("symfony")->isRequired()->cannotBeEmpty()->end()
					//the location of the contract.json file that holds the service api contract information
					->scalarNode('contract')->isRequired()->cannotBeEmpty()->end()
					//A reachable IP address or URL that the gateway will use to communicate with this application
					//NOTE: if URL is provided, it must use HTTPS and do not add HTTPS to the value
					//Example: URL is https://www.myapp.com --> Provider: www.myapp.com (only)
					->scalarNode('ip')->defaultValue("127.0.0.1")->isRequired()->cannotBeEmpty()->end()
					//The hosting platform type (manual / docker / kubernetes)
					->scalarNode('platform')->defaultValue("manual")->isRequired()->cannotBeEmpty()->end()
					//if platform is docker or kubernetes, provide the options
					->arrayNode('platformOptions')
						->children()
							->scalarNode('service')->end()
							->scalarNode('namespace')->end()
							->scalarNode('exposedPort')->end()
							->scalarNode('network')->end()
						->end()
				->end()
			->end();

		return $treeBuilder;
	}
}