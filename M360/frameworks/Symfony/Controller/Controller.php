<?php

namespace M360\frameworks\Symfony\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use M360\frameworks\Symfony\Service\M360MiddlewareService;

class Controller extends AbstractController
{
	private M360MiddlewareService $M360mw;

	public function __construct (M360MiddlewareService $m360MiddlewareService) {
		$this->M360mw = $m360MiddlewareService;
		$this->M360mw->init();
	}

	/**
	 * @Route("/heartbeat", name="heartbeat")
	 */
	public function heartbeat (): Response {
		return new Response(json_encode([
			"name" => $this->M360mw->getAppName(),
			"heartbeat" => true
		], JSON_PRETTY_PRINT), 200, ['Content-Type' => 'application/json']);
	}

	/**
	 * @Route("/registry/reload", name="reload registry")
	 * @param Request $request
	 * @return Response
	 */
	public function registryReload (Request $request): Response {
		$registry = $this->M360mw->getAppMaintenance()->reloadRegistry($request);
		if (!empty($registry)) {
			return new Response(json_encode($registry, JSON_PRETTY_PRINT), 200, ['Content-Type' => 'application/json']);
		} else {
			return new Response(json_encode(['error' => 'Error reloading the registry!']), 200, ['Content-Type' => 'application/json']);
		}
	}

	/**
	 * @Route("/awareness/reload", name="reload awareness")
	 * @param Request $request
	 * @return Response
	 */
	public function awarenessReload (Request $request): Response {
		$awareness = $this->M360mw->getAppMaintenance()->reloadAwareness($request);
		if (!empty($awareness)) {
			return new Response(json_encode($awareness, JSON_PRETTY_PRINT), 200, ['Content-Type' => 'application/json']);
		} else {
			return new Response(json_encode(['error' => 'Error reloading the awareness!']), 200, ['Content-Type' => 'application/json']);
		}
	}
}