<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360;

class Tenant
{
	private $GatewayConnector;

	public function __construct () {

	}

	/**
	 * Will receive the gateway connector object
	 * @param type $gatewayConnector
	 */
	public function setGatewayConnector ($gatewayConnector) {
		$this->GatewayConnector = $gatewayConnector;
	}

	/**
	 * This methods checks that m360 is provided in the request headers.
	 * Then it scans for the requested tenant inside m360 and returns it.
	 * @param {Object} $request
	 * @param {String} $code
	 */
	public function get ($request, $code = "") {
		if (trim($code) == "") {
			return null;
		}

		if (array_key_exists("m360", $request) && !empty($request["m360"])) {
			$m360 = json_decode($request["m360"], false);
			$tenant = null;
			if (!empty($m360) && property_exists($m360, "tenants") && is_array($m360->tenants)) {
				foreach ($m360->tenants as $oneTenant) {
					if ($oneTenant->code == $code) {
						$tenant = $oneTenant;
					}
				}
			}
			return $tenant;
		}
		return null;
	}

	/**
	 * this method checks that m360 is provided in the request headers.
	 * Then it returns the list of all the tenants it finds in it.
	 * @param {Object} $request
	 */
	public function list ($request) {
		if (array_key_exists("m360", $request)) {
			$m360 = json_decode($request["m360"], false);
			if (!empty($m360) && property_exists($m360, "tenants") && is_array($m360->tenants)) {
				return $m360->tenants;
			}
		}
		return false;
	}

	/**
	 * Returns the list of tenants from the database based on the codes provided as payload
	 * @param {Array} $codes
	 */
	public function find ($codes) {
		$body = $codes;
		$params = array(
			"method" => "post",
			"decrypt" => "true",
			"route" => "/tenants",
			"body" => $body
		);
		return $this->GatewayConnector->invoke($params);
	}
}
