<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360;

// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_DEPRECATED);

use Exception;
use JsonSchema\Validator;

/**
 * Middleware SDK Class provides a wrapper around all the Middleware Functionality
 * @package M360
 */
class SDK
{

	public $schema;
	public $validator;
	public $GatewayConnector;
	public $registry;
	public $database;
	public $resource;
	public $custom;
	public $configuration;
	public $service;
	public $logger;
	public $awareness;
	public $user;
	public $tenant;
	public $rbac;

	public function __construct(){

	}

	/**
	 * Initialize the middleware and validate the provided configuration
	 * @param {Object} configuration
	 * @return {Exception}
	 */
	public function init ($configuration, Registry $registryModule = null, Awareness $awarenessModule = null, Service $serviceModule = null) {

		if(!empty($registryModule)){
			$this->registry = $registryModule;
		}
		else{
			$this->registry = new Registry();
		}

		if(!empty($awarenessModule)) {
			$this->awareness = $awarenessModule;
		}
		else{
			$this->awareness = new Awareness();
			$this->awareness->init();
		}

		if(!empty($serviceModule)) {
			$this->service = $serviceModule;
		}
		else{
			$this->service = new Service();
		}

		//validate configuration data type
		if (gettype($configuration) !== "object") {
			throw new Exception("The configuration provided is not well formatted! Please make sure you're using an Object instead of an Array");
		}

		//load the json validation schema
		$this->schema = new Schema();
		$schema = $this->schema->getMWConfig(true);

		//create new json validator
		$this->validator = new Validator();

		//validate that the contract provided is json otherwise try to parse it
		if (gettype($configuration->contract) == 'string') {
			$configuration->contract = json_decode($configuration->contract, false);
		}

		//validate the structure of the configuration provided
		$this->validator->validate($configuration, $schema);
		if (!$this->validator->isValid()) {
			$errors = [];
			foreach ($this->validator->getErrors() as $error) {
				array_push($errors, "Error in $error[property] => $error[message]");
			}
			throw new Exception(implode("\n", $errors));
		}

		//all works, assign the configuraiton as a property
		$this->configuration = $configuration;

		//initialize the rbac module
		$this->rbac = new RBAC();

		if ($configuration->type == "job") {
			return $this->initJob();
		} else {
			$this->initServer();
			$this->augment();
		}
	}

	private function initServer () {
		/**
		 * Check the platform and its options
		 */
		if (in_array($this->configuration->platform, ['docker', 'kubernetes'])) {
			if (empty($this->configuration->platformOptions)) {
				throw new Exception("Missing \"platformOptions\" for platform '{$this->configuration->platform}'. Please refer to the documentation for additional help!");
			}

			$errors = [];
			switch ($this->configuration->platform) {
				case 'docker':
					// 'network': 'mike',
					// 'service': 'service-express',
					if (empty($this->configuration->platformOptions['network'])) {
						array_push($errors, 'Missing docker "network" name in platform options.');
					}
					if (empty($this->configuration->platformOptions['service'])) {
						array_push($errors, 'Missing docker "service" name in platform options.');
					}

					break;
				case 'kubernetes':
					// 'namespace': 'mike',
					// 'service': 'service-express',
					// 'exposedPort': process.env.APP_PORT || 4002,
					if (empty($this->configuration->platformOptions['namespace'])) {
						array_push($errors, 'Missing kubernetes "namespace" value in platform options.');
					}
					if (empty($this->configuration->platformOptions['service'])) {
						array_push($errors, 'Missing kubernetes "service" name in platform options.');
					}

					if (empty($this->configuration->platformOptions['exposedPort'])) {
						array_push($errors, 'Missing kubernetes "exposedPort" value for container in platform options.');
					}
					break;
			}

			if (sizeof($errors) > 0) {
				throw new Exception(implode("\n\t", $errors));
			}
		}

		/**
		 * Check if preserved routes are overridden
		 */
		$reserved = array('/heartbeat', '/registry/reload', '/awareness/reload');

		foreach ($this->configuration->contract->apis as $apiObject => $apiType) {
			foreach ($apiType as $method => $methodArray) {
				foreach ($methodArray as $apiRouteInfo => $settings) {
					if (strtolower($method) == "get" && in_array($apiRouteInfo, $reserved)) {
						throw new Exception("Route [GET] '{ $apiRouteInfo }' is reserved by the middleware, please use a different route or method for your endpoint. ");
					}
				}
			}
		}

		return true;
	}

	/**
	 * Initialize the middleware for jobs and validate the provided configuration
	 * @param {Object} configuration
	 * @return {Exception}
	 */
	private function initJob () {
		echo("\n-> Retrieving Service Contract from M360 Gateway ...");
		$this->service->init($this->configuration);
		$response = $this->service->get();
		echo("\n-> Service Found in M360 Gateway...");

		//initialize the middleware sdk gateway connector driver
		$this->GatewayConnector = new gateway\Connector(
			$this->configuration->contract->name,
			$this->configuration->contract->version,
			!empty($this->configuration->gatewayMaintenanceIP) ? $this->configuration->gatewayMaintenanceIP : null,
			!empty($this->configuration->gatewayMaintenancePort) ? $this->configuration->gatewayMaintenancePort : null
		);

		// $this->registry = new Registry($this->GatewayConnector,$this->crypter);
		$this->registry->setGatewayConnector($this->GatewayConnector);

		//attach the remaining modules
		$this->database = new Databases();
		$this->resource = new Resource();
		$this->custom = new CustomRegistry();

		$this->user = new User();
		$this->user->setGatewayConnector($this->GatewayConnector);

		$this->tenant = new Tenant();
		$this->tenant->setGatewayConnector($this->GatewayConnector);

		//get the registry from the gateway and cache it
		$registry = $this->registry->get("registry", "");
		//initialize the awareness sdk module
		if (!empty($registry)) {
			$this->awareness->init($registry->_TTL);
			$this->awareness->setGatewayConnector($this->GatewayConnector);

			echo("\n-> Connection to M360 Gateway established...");
			echo("\n-> Environment Registry loaded from M360 Gateway and cached...\n");

			//Auto-wire the reload registry procedure with a default TTL of 30 seconds
			$this->logger = new Logger($registry);
		} else {
			$this->logger = new Logger("");
		}

		return true;
	}

	/**
	 * Augment the default configuration of the service contract and add the M360 maintenance routes to it.
	 * Augment the default microservice apis and add the M360 cloud awareness routes to the server instance.
	 * @param {Object} configuration
	 */
	public function augment () {
		/**
		 * Update the service contract with preserved maintenance entries.
		 */
		$this->configuration->contract->heartbeat = (object)[
			"method" => "get",
			"route" => "/heartbeat",
			"access" => false,
			"label" => "Heartbeat"
		];
		$this->configuration->contract->registry = (object)[
			"method" => "get",
			"route" => "/registry/reload",
			"access" => false,
			"label" => "Reload Registry"
		];

		if (empty($this->configuration->contract->apis->maintenance)) {
			$this->configuration->contract->apis->maintenance = (object)[];
		}

		if (empty($this->configuration->contract->apis->maintenance->get)) {
			$this->configuration->contract->apis->maintenance->get = (object)[];
		}

		$this->configuration->contract->apis->maintenance->get->{"/awareness/reload"} = array(
			"access" => false,
			"label" => "Reload Cached Awareness"
		);

		//set the rbac configuration
		$this->rbac->setServiceConfig($this->configuration);

		//initialize the middleware sdk gateway connector driver
		$this->GatewayConnector = new gateway\Connector(
			$this->configuration->contract->name,
			$this->configuration->contract->version,
			!empty($this->configuration->gatewayMaintenanceIP) ? $this->configuration->gatewayMaintenanceIP : null,
			!empty($this->configuration->gatewayMaintenancePort) ? $this->configuration->gatewayMaintenancePort : null
		);

		$this->registry->setGatewayConnector($this->GatewayConnector);
		$this->awareness->setGatewayConnector($this->GatewayConnector);
		$this->service->init($this->configuration);
	}

	/**
	 * Method that triggers the auto registration procedure and registers/updates the microservice in the gateway.
	 * Once registered, the gateway will be able to proxy requests to it.
	 * @return {boolean} Exception if error is thrown
	 */
	public function autoRegister () {
		//auto register the service at the gateway unless there is an env to stop it.
		$autoRegister = in_array("APP_AUTOREGISTER", $_SERVER) ? ($_SERVER["APP_AUTOREGISTER"] == 'true') : true;
		if ($autoRegister) {
			$this->service->ensureIPAddress();

			echo("\n-> Registering/Updating Service Contract in M360 Gateway ...");
			$response = $this->service->register();
			echo("\n-> Service Registered in M360 Gateway...");

			//get the registry from the gateway and cache it
			$registry = $this->registry->get("registry");

			//if the registry was loaded ...
			if (!empty($registry)) {
				//initialize the awareness sdk module
				$this->awareness->resetTTL($registry->_TTL);

				//display all registration messages to the terminal.
				if ($response && $response->notifications && count($response->notifications) > 0) {
					echo("\n\n*********************");
					echo("\nRegistration Details");
					echo("\n*********************\n");
					echo(implode("\n", $response->notifications));
				}

				echo("\n-> Connection to M360 Gateway established...");
				echo("\n-> Environment Registry loaded from M360 Gateway and cached...\n");

				//set the logger
				$this->logger = new Logger($registry);
			} else {
				//set the logger
				$this->logger = new Logger("");
			}

			//attach the remaining modules
			$this->database = new Databases();
			$this->resource = new Resource();
			$this->custom = new CustomRegistry();

			$this->user = new User();
			$this->tenant = new Tenant();
		}
	}
}
