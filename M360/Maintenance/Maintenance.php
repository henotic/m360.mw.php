<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360\Maintenance;

class Maintenance
{
	private $curl;
	private $k8sToken;

	public function __construct ($appName, $configuration, $Registry, $Service, $Awareness, \Curl\Curl $curl = null, $k8sToken = null) {
		$this->configuration = $configuration;
		$this->appName = $appName;
		$this->Registry = $Registry;
		$this->Service = $Service;
		$this->Awareness = $Awareness;

		if(!empty($curl)){
			$this->curl = $curl;
		}
		else{
			$this->curl = new \Curl\Curl();
		}

		if(!empty($k8sToken)){
			$this->k8sToken = $k8sToken;
		}
		else{
			$this->k8sToken = '/var/run/secrets/kubernetes.io/serviceaccount/token';
		}
	}

	/**
	 * Private method that returns the list of ignored entries from the request and supports mapping data from either
	 * laravel or symfony
	 * @param $request
	 * @return array|mixed
	 */
	private function checkGetIgnoreList($request){
		$ignoreList = [];

		//support laravel
		if(method_exists($request, 'query')){
			$ignoreList = (!empty($request->query('ignoreList'))) ? $request->query('ignoreList') : [];
		}
		//support symfony
		else if(property_exists($request, 'query')){
			$ignoreList = (!empty($request->query->get('ignoreList'))) ? $request->query->get('ignoreList') : [];
		}

		return $ignoreList;
	}

	/**
	 * Public method that wraps around the reload registry operation
	 * @param {Object} request
	 */
	public function reloadRegistry ($request) {
		$registry = $this->Registry->reload();
		if (!$registry) {
			return false;
		}

		/**
		 * Load the other replicas and trigger their reload registry as well
		 * @param {Function} aCb
		 */

		$ignoreList = $this->checkGetIgnoreList($request);
		$data = $this->getBrothers($ignoreList);
		if (!$data) {
			return false;
		}

		if (sizeof($data->brothers) === 0) {
			return $registry;
		}

		//update brothers
		$internalMaintenancePort = $this->configuration->contract->ports->maintenance;
        $protocol = 'http';
        if(isset($this->configuration->contract->host) && isset($this->configuration->contract->ssl)) {
          $protocol = $this->configuration->contract->ssl ? 'https': 'http';
        }
		foreach ($data->brothers as $key => $oneBother) {
			$brotherURL = $protocol . '://' . $oneBother . ':' . $internalMaintenancePort . '/registry/reload';
			$curl = $this->curl;

			$curl->setHeader("Content-Type", "application/json");
			$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
			$curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);
			$params = [
				"ignoreList" => $data->ignoreList
			];
			$curl->get($brotherURL, $params, true);
			$response = $curl->getResponse();
		}

		//al done
		return $registry;
	}

	/**
	 * Public method that wraps around the reload awarenes operation
	 * @param {Object} query
	 */
	public function reloadAwareness ($request) {

		$awareness = $this->Awareness->reload();
		if (!$awareness) {
			return false;
		}

		$ignoreList = $this->checkGetIgnoreList($request);
		$data = $this->getBrothers($ignoreList);

		if (!$data) {
			return false;
		}

		if (sizeof($data->brothers) === 0) {
			return $awareness;
		}

		//update brothers
		$internalMaintenancePort = $this->configuration->contract->ports->maintenance;
        $protocol = 'http';
        if(isset($this->configuration->contract->host) && isset($this->configuration->contract->ssl)) {
            $protocol = $this->configuration->contract->ssl ? 'https': 'http';
        }
		foreach ($data->brothers as $key => $oneBother) {
			$brotherURL = $protocol . '://' . $oneBother . ':' . $internalMaintenancePort . '/awareness/reload';
			$curl = $this->curl;

			$curl->setHeader("Content-Type", "application/json");
			$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
			$curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);
			$params = [
				"ignoreList" => $data->ignoreList
			];
			$curl->get($brotherURL, $params, true);
			$response = $curl->getResponse();
		}

		//al done
		return $awareness;
	}

	/**
	 * Private method that generates the list of adjacent replicas in containerized deployment
	 * remove the ips from the list if they are in the ignoreList, at the end push the current ip to the ignorelist
	 * @param {Array} ignoreList
	 */
	private function getBrothers ($ignoreList) {

		if (!in_array($this->configuration->platform, ['docker', 'kubernetes'])) {
			return (object)['brothers' => []];
		}

		//get the current host ip and added it to the ignore list
		$currentHostIP = $this->getCurrentIPAddress();
		array_push($ignoreList, $currentHostIP);

		if ($this->configuration->platform == 'kubernetes') {
			//".subsets[].addresses[].ip"
			/*
				TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token); \
				curl https://kubernetes.default.svc/api/v1/namespaces/${namespace}/endpoints/${service-name} \
				   --header "Authorization: Bearer $TOKEN" --insecure
			 */
			$content = file_get_contents($this->k8sToken);
			if (!$content) {
				return false;
			}
			$k8sToken = $content;
			$curl = $this->curl;

			$curl->setHeader("Content-Type", "application/json");
			$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
			$curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);
			$curl->setHeader("Authorization", "Bearer " . $k8sToken);
			$newURL = "https://kubernetes.default.svc/api/v1/namespaces/{$this->configuration->platformOptions['namespace']}/endpoints/{$this->configuration->platformOptions['service']}";
			$curl->get($newURL, array(), true);
			$response = json_decode($curl->getResponse(), false);
			if (!$response) {
				return false;
			}

			$IPAddresses = [];

			//extract the hosts ip addresses
			if (!empty($response) && !empty($response->subsets)) {
				$hostIP = '';
				foreach ($response->subsets as $key => $oneSubset) {
					if (!empty($oneSubset) && !empty($oneSubset->addresses) && is_array($oneSubset->addresses)) {
						foreach ($oneSubset->addresses as $aKey => $oneAddress) {
							$hostIP = $oneAddress->ip;

							//if this host entry is not in the ignore list, then added to tbe be processed, else skip it
							if (!in_array($hostIP, $ignoreList)) {
								array_push($IPAddresses, $hostIP);
							}
						}
					}
				}
			}
			return (object)([
				'brothers' => $IPAddresses,
				'ignoreList' => $ignoreList
			]);

		} else if ($this->configuration->platform === 'docker') {
			//".[].NetworkSettings.Networks.mike.IPAddress"
			/*
				curl --unix-socket /var/run/docker.sock \
				-X GET http://v2/containers/json?filters=%7B%22label%22%3A%5B%22com.docker.swarm.service.name%3Dmy_service%22%5D%7D
			 */

			$filters = urlencode(json_encode("{\"label\":[\"com.docker.swarm.service.name={$this->configuration->platformOptions['service']}\"]}"));

			$curl = $this->curl;

			$curl->setHeader("Content-Type", "application/json");
			$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
			$curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);
			$curl->setOpt(CURLOPT_UNIX_SOCKET_PATH, '/var/run/docker.sock');

			$newURL = 'http://v2/containers/json';
			$response = $curl->get($newURL, array('filters' => $filters), true);
			if (!$response) {
				return false;
			}

			$IPAddresses = [];

			//extract the hosts ip addresses
			if (!empty($response) && is_array($response)) {
				foreach ($response as $key => $oneContainer) {
					$hostIP = '';
					if ($oneContainer->NetworkSettings &&
						$oneContainer->NetworkSettings->Networks &&
						$oneContainer->NetworkSettings->Networks[$this->configuration->platformOptions['network']]
					) {
						$hostIP = $oneContainer->NetworkSettings->Networks[$this->configuration->platformOptions['network']]->IPAddress;

						//if this host entry is not in the ignore list, then added to tbe be processed, else skip it
						if (!in_array($hostIP, $ignoreList)) {
							array_push($IPAddresses, $hostIP);
						}
					}
				}
			}

			return (object)([
				'brothers' => $IPAddresses,
				'ignoreList' => $ignoreList
			]);
		}
	}

	/**
	 * Private method that returns the ip address of the current host
	 */
	private function getCurrentIPAddress () {
		$cmd = (PHP_OS === 'Darwin') ? "hostname" : "hostname -i";
		exec($cmd, $hostIP);
		if (!empty($hostIP)) {
			$hostIP = (is_array($hostIP)) ? $hostIP[0] : $hostIP;
			$hostIP = trim($hostIP);
		}
		return (!empty($hostIP)) ? $hostIP : '127.0.0.1';
	}
}