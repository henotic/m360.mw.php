<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360;

use M360\Tenant;
use M360\User;
use M360\Service;
use M360\Utils;
use Symfony\Component\HttpFoundation\Request;

class RBAC
{
	private $config;

	private $service;
	private $user;
	private $tenant;
	private $utils;

	public function __construct () {
		$this->utils = new Utils();
		$this->service = new Service();
		$this->user = new User();
		$this->tenant = new Tenant();
	}

	/**
	 * This method sets the configuration of the middleware in the RBAC library
	 * @param {Object} $configuration
	 */
	public function setServiceConfig ($configuration) {
		$this->config = $configuration;
	}

	public function resetComponents (User $user = null, Tenant $tenant = null) {
		if (!empty($user) && $user instanceof User) {
			$this->user = $user;
		}

		if (!empty($tenant) && $tenant instanceof Tenant) {
			$this->tenant = $tenant;
		}
	}

	/**
	 * This methods checks that m360 is provided in the request headers.
	 * Then it scans to find the fields information inside m360 and returns it.
	 * @param {Object} $request
	 * @param {String} $section
	 * @returns {*}
	 */
	public function get ($request, $section = null) {
		if (!empty($request) && array_key_exists("m360", $request) && !empty($request["m360"])) {
			$m360 = json_decode($request["m360"], false);

			if (empty($m360) || empty($m360->rbac)) {
				return null;
			}

			switch ($section) {
				case 'resource':
				case 'resources':
					$section = 'resources';
					break;
				case 'condition':
				case 'conditions':
					$section = 'conditions';
					break;
				case 'field':
				case 'fields':
					$section = 'fields';
					break;
			}

			if (!empty($section) && trim($section) !== '' && $m360->rbac->$section) {
				return $m360->rbac->$section;
			}

			return $m360->rbac;
		}

		return null;
	}

	/**
	 * simplified method that wraps around the other can Access operations
	 * @param {Object} $request
	 * @param {String} $mode
	 * @param {Object} $data
	 * @returns {boolean|*}
	 */
	public function canAccess ($request, $mode, $data) {
		$options = null;
		switch ($mode) {
			case 'resource':
			case 'resources':
				$options = $this->get($request, 'resources');
				return $this->canAccessResources($request, $options, $data);
			case 'condition':
			case 'conditions':
				$options = $this->get($request, 'conditions');
				return $this->canAccessConditions($request, $options, $data);
			case 'field':
			case 'fields':
				$options = $this->get($request, 'fields');
				return $this->canAccessFields($request, $options, $data);
			default:
				throw new \Exception("Unsupported RBAC Access Mode Parameter 2!");
		}
	}

	/**
	 * @param {Object} $oneDataObject
	 * @param {Object} $RBACOptions
	 * @return {Boolean} bool
	 */
	private function checkAllFieldsInObject_canAccessFields ($oneDataObject, $RBACOptions) {
		//loop in configured field and for each entry check in the object
		foreach ($oneDataObject as $key => $value) {

			//ignore the non contract fields
			if (!in_array($key, $RBACOptions->contract)) {
				continue;
			}

			//return false immediately if access is denied to any of the fields
			if (in_array($key, $RBACOptions->deny)) {
				return false;
			}

		}

		//return ok, all normal
		return true;
	}

	/**
	 * @param {Object} $RBACOptions
	 * @return {Boolean}
	 */
	private function isFieldRBACConfigured_canAccessFields ($RBACOptions) {
		if (
			!empty($RBACOptions) &&
			isset($RBACOptions->operator) &&
			in_array($RBACOptions->operator, ['allow', 'deny']) &&
			isset($RBACOptions->list) &&
			(sizeof(array_keys(get_object_vars($RBACOptions->list))) > 0)
		) {
			return true;
		}

		return false;
	}

	/**
	 * This method checks if the access is granted or denied for the given data object based on the given options
	 * @param {Object} $request
	 * @param {Object} $options
	 * @param {Object} $data
	 * @returns {boolean}
	 */
	public function canAccessFields ($request, $options, $data) {
		//get the list of configured fields
		$RBACOptions = clone $options;

		if (empty($data) || is_array($data) || !is_object($data)) {
			throw new \Exception("Unsupported data type provided, parameter 3 should can only be of type Object");
		}

		//default to true if no RBAC fields is configured
		if (!$this->isFieldRBACConfigured_canAccessFields($RBACOptions)) {
			return true;
		}

		$RBACOptions = $this->getRBACFieldsFromServiceContract($request, $RBACOptions);
		return $this->checkAllFieldsInObject_canAccessFields($data, $RBACOptions);
	}

	/**
	 * @param {Object} $data
	 * @param $user
	 * @param {Object} $options
	 * @param $utils
	 * @return bool
	 */
	private function checkResourcesAccessInObject_canAccessResources ($data, $user = null, $options, Utils $utils) {
		//if no user, return false
		if (empty($user)) {
			return false;
		}

		//get the value from the data record
		$leftFieldValue = $utils->checkIfKeyIsInObjectAndReturnValue($data, $options->field);

		//get the value from the request user object
		$rightFieldValue = $utils->checkIfKeyIsInObjectAndReturnValue($user, $options->value);

		//return true if both match
		return ($leftFieldValue == $rightFieldValue);

	}

	/**
	 * @param {Object} $options
	 * @return {Boolean} bool
	 */
	private function isFieldRBACConfigured_canAccessResources ($options) {

		if (
			!empty($options) &&
			property_exists($options, "mode") &&
			in_array($options->mode, ['any', 'own'])
		) {

			if ($options->mode == 'any') {
				return false;
			} else if (
				$options->mode == 'own' &&
				property_exists($options, "field") &&
				property_exists($options, "value") &&
				trim($options->field) != '' &&
				trim($options->value) != ''
			) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	/**
	 * This method checks if the user in the request has access to the data record provided based on Configured RBAC Resources
	 * @param {Object} $request
	 * @param {Object} $options
	 * @param {data} $data
	 * @returns {boolean}
	 */
	public function canAccessResources ($request, $options, $data) {

		if (empty($data) || is_array($data) || !is_object($data)) {
			throw new \Exception("Unsupported data type provided, parameter 3 should can only be of type Object");
		}

		if ($this->isFieldRBACConfigured_canAccessResources($options)) {
			$user = $this->user->get($request);
			return $this->checkResourcesAccessInObject_canAccessResources($data, $user, $options, $this->utils);
		} else {
			//default to true if no RBAC fields is configured
			return true;
		}
	}

	/**
	 * @param $user
	 * @param $oneCondition
	 * @param $data
	 * @param $utils
	 * @param $tenant
	 * @param $request
	 * @return bool
	 */
	private function checkOneCondition_canAccessConditions ($user, $oneCondition, $data, Utils $utils, $tenant, $request) {
		//get the value from the data record
		$leftFieldValue = $utils->checkIfKeyIsInObjectAndReturnValue($data, $oneCondition->arguments->field);
		$rightFieldValue = null;

		if (
			property_exists($oneCondition->arguments, 'tCode') &&
			trim($oneCondition->arguments->tCode) != ''
		) {
			//if no tenant, return false
			$tenant = $tenant->get($request, $oneCondition->arguments->tCode);
			if (empty($tenant)) {
				return false;
			}

			$leftFieldValue = $utils->checkIfKeyIsInObjectAndReturnValue($tenant, $oneCondition->arguments->field);
		}

		//get the value from the request user object
		$value = $oneCondition->arguments->value;
		if (
			property_exists($oneCondition->arguments, 'custom') &&
			trim($oneCondition->arguments->custom) != ''
		) {
			$rightFieldValue = $oneCondition->arguments->custom;
		} else {
			$rightFieldValue = $utils->checkIfKeyIsInObjectAndReturnValue($user, $value);
		}

		if (in_array($rightFieldValue, ['true', 'false'])) {
			$rightFieldValue = ($rightFieldValue == 'true');
		}


		$conditionStatusResult = false;
		//return true if function matches
		switch (strtolower($oneCondition->function)) {
			case 'empty':
				if (empty($leftFieldValue)) {
					$conditionStatusResult = true;
				}
				break;
			case 'not_empty':
				if (!empty($leftFieldValue)) {
					$conditionStatusResult = true;
				}
				break;
			case 'eq':
				if ($leftFieldValue == $rightFieldValue) {
					$conditionStatusResult = true;
				}
				break;
			case 'not_eq':
				if ($leftFieldValue != $rightFieldValue) {
					$conditionStatusResult = true;
				}
				break;
			case 'start':
				if (str_starts_with($leftFieldValue, $rightFieldValue)) {
					$conditionStatusResult = true;
				}
				break;
			case 'not_start':
				if (!str_starts_with($leftFieldValue, $rightFieldValue)) {
					$conditionStatusResult = true;
				}
				break;
			case 'end':
				if (str_ends_with($leftFieldValue, $rightFieldValue)) {
					$conditionStatusResult = true;
				}
				break;
			case 'not_end':
				if (!str_ends_with($leftFieldValue, $rightFieldValue)) {
					$conditionStatusResult = true;
				}
				break;
			case 'in':
			case 'contain':
				if (str_contains($leftFieldValue, $rightFieldValue)) {
					$conditionStatusResult = true;
				}
				break;
			case 'not_in':
			case 'not_contain':
				if (!str_contains($leftFieldValue, $rightFieldValue)) {
					$conditionStatusResult = true;
				}
				break;
		}

		return $conditionStatusResult;

	}

	/**
	 * @param $options
	 * @return bool
	 */
	private function isFieldRBACConfigured_checkConditions ($options) {
		if (
			!empty($options) &&
			property_exists($options, 'operator') &&
			in_array($options->operator, ['$and', '$or']) &&
			property_exists($options, 'criteria') &&
			is_array($options->criteria) &&
			sizeof($options->criteria) > 0
		) {
			return true;
		}
		return false;
	}

	/**
	 * @param $options
	 * @param \M360\User $user
	 * @param $allowedMethods
	 * @param $data
	 * @param \M360\Utils $utils
	 * @param \M360\Tenant $tenant
	 * @param Request $request
	 * @return bool|\Exception
	 */
	private function checkConfiguredConditionsforAccessInObject ($options, $user, $allowedMethods, $data, Utils $utils, $tenant, $request) {

		//if no user, return false
		if (empty($user)) {
			return false;
		}

		$conditionsMet = [];
		for ($i = 0; $i < sizeof($options->criteria); $i++) {
			$oneCriteria = $options->criteria[$i];

			if (
				property_exists($oneCriteria, 'function') &&
				in_array($oneCriteria->function, $allowedMethods) &&
				property_exists($oneCriteria, 'arguments') &&
				property_exists($oneCriteria->arguments, 'field') &&
				property_exists($oneCriteria->arguments, 'value')
			) {
				try {
					array_push($conditionsMet, $this->checkOneCondition_canAccessConditions($user, $oneCriteria, $data, $utils, $tenant, $request));
				} catch (\Exception $error) {
					throw $error;
				}
			} else {
				//return false immediately if one condition doesn't have the correct format
				return false;
			}

		}

		//if operator is and the array must have all true
		if ($options->operator == '$and') {
			return !in_array(false, $conditionsMet);
		} else {
			return in_array(true, $conditionsMet);
		}
	}

	/**
	 * This method checks if the user in the request has access to the data record provided based on Configured RBAC Conditions
	 * @param {Object} $request
	 * @param {Object} $options
	 * @param {Object} $data
	 * @returns {boolean}
	 */
	public function canAccessConditions ($request, $options, $data) {

		$allowedMethods = [
			"EMPTY",
			"NOT_EMPTY",
			"EQ",
			"NOT_EQ",
			"START",
			"NOT_START",
			"END",
			"NOT_END",
			"IN",
			"NOT_IN",
			"CONTAIN",
			"NOT_CONTAIN"
		];

		if (empty($data) || is_array($data) || !is_object($data)) {
			throw new \Exception("Unsupported data type provided, parameter 3 should can only be of type Object");
		}

		if ($this->isFieldRBACConfigured_checkConditions($options)) {
			$user = $this->user->get($request);
			return $this->checkConfiguredConditionsforAccessInObject($options, $user, $allowedMethods, $data, $this->utils, $this->tenant, $request);
		} else {
			//default to true if no RBAC fields is configured
			return true;
		}
	}

	/**
	 * @param $record
	 * @param $options
	 * @param \M360\Utils $utils
	 * @return array|mixed
	 */
	private function doFilter ($record, $options, Utils $utils) {
		$returnRecord = [];

		if ($options->operator == 'deny') {
			$returnRecord = clone $record;

			foreach ($options->deny as $deniedField) {
				if (str_contains($deniedField, '.')) {
					$utils->removeObjectFromPath($returnRecord, $deniedField);
				} else {
					if(is_object($returnRecord)){
						unset($returnRecord->$deniedField);
					}
					else
						unset($returnRecord[$deniedField]);
				}
			}
		} else {

			//fill the fields that are not in the contract
			foreach ($record as $i => $val) {
				if (!in_array($i, $options->contract)) {
					$returnRecord[$i] = (is_object($val)) ? clone $val: $val;
				}
			}

			foreach ($options->allow as $val) {
				if (str_contains($val, '.')) {
					$fieldValue = $utils->checkIfKeyIsInObjectAndReturnValue($record, $val);
					$fieldNamePath = explode(".", $val);
					if(!isset($returnRecord[$fieldNamePath[0]])){
						$returnRecord[$fieldNamePath[0]] = [];
					}
					$returnRecord[$fieldNamePath[0]] = $utils->buildObjectFromPath($returnRecord, $val, $fieldValue);
				} else {
					if(is_array($record)) {
						$returnRecord[$val] = $record[$val];
					}

					if (is_object($record) ){
						$returnRecord[$val] = (is_object($record->$val)) ? clone $record->$val : $record->$val;
					}
				}
			}
		}

		return $returnRecord;
	}

	/**
	 * @param $options
	 * @return bool
	 */
	private function isFieldRBACConfigured_filterFields ($options) {
		if (
			!empty($options) &&
			property_exists($options, "operator") &&
			in_array($options->operator, ['allow', 'deny']) &&
			property_exists($options, 'list') &&
			(sizeof(array_keys((array)$options->list)) > 0)
		) {
			return true;
		}
		return false;
	}

	/**
	 * This method removes the fields from the record if the access is false
	 * @param {Object} $request
	 * @param {Array|Object} $data
	 * @returns {Array|Object}
	 */
	public function filterFields ($request, $data) {
		$RBACOptions = $this->get($request, 'fields');

		$options = $this->getRBACFieldsFromServiceContract($request, $RBACOptions);
		if (!$this->isFieldRBACConfigured_filterFields($options)) {
			return $data;
		}

		if (is_array($data)) {
			for ($i = 0; $i < sizeof($data); $i++) {
				$data[$i] = $this->doFilter($data[$i], $options, $this->utils);
			}
		} else if (is_object($data)) {
			$data = $this->doFilter($data, $options, $this->utils);
		}

		return $data;
	}

	/**
	 * This method checks if the rbac fields configuration is set for a specific endpoint in the service contract
	 * by cross referencing the request method and url with the contract
	 * @param {Object} $request
	 * @param {Object} $rbacFieldsOptions
	 * @returns {null|Object}
	 */
	private function getRBACFieldsFromServiceContract ($request, &$rbacFieldsOptions) {

		$endPointInfo = $this->service->getEndpointInfo($request);
		$method = strtolower($endPointInfo->method);
		$endpoint = $endPointInfo->endpoint;

		$rbacFieldsOptions->allow = [];
		$rbacFieldsOptions->deny = [];

		if (
			isset($this->config->contract) &&
			isset($this->config->contract->apis) &&
			isset($this->config->contract->apis->main) &&
			isset($this->config->contract->apis->main->$method) &&
			isset($this->config->contract->apis->main->$method->$endpoint) &&
			isset($this->config->contract->apis->main->$method->$endpoint->rbac)
		) {
			$fields = $this->config->contract->apis->main->$method->$endpoint->rbac->fields;
			$rbacFieldsOptions->contract = $fields;
			$rbacConfiguredFieldList = clone $rbacFieldsOptions->list;
			$processedFields = [];

			foreach ($fields as $key => $oneContractField) {

				foreach ($rbacConfiguredFieldList as $i => $oneRbacConfiguredFieldList) {
					if (
						$oneRbacConfiguredFieldList == $oneContractField ||
						str_contains($oneRbacConfiguredFieldList, $oneContractField)
					) {
						if ($rbacFieldsOptions->operator == 'allow') {
							if (!in_array($oneRbacConfiguredFieldList, $rbacFieldsOptions->allow)) {
								array_push($rbacFieldsOptions->allow, $oneRbacConfiguredFieldList);
							}
						} else {
							if (!in_array($oneRbacConfiguredFieldList, $rbacFieldsOptions->deny)) {
								array_push($rbacFieldsOptions->deny, $oneRbacConfiguredFieldList);
							}
						}

						unset($rbacConfiguredFieldList->$i);
						array_push($processedFields, $oneContractField);
					}
				}
			}

			foreach ($fields as $key => $oneContractField) {

				if (!in_array($oneContractField, $processedFields)) {

					if ($rbacFieldsOptions->operator == 'allow') {
						if (!in_array($oneContractField, $rbacFieldsOptions->deny)) {
							array_push($rbacFieldsOptions->deny, $oneContractField);
						}
					} else {
						if (!in_array($oneContractField, $rbacFieldsOptions->allow)) {
							array_push($rbacFieldsOptions->allow, $oneContractField);
						}
					}
				}
			}
		}

		return $rbacFieldsOptions;
	}
}

