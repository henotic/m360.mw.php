<?php

namespace M360;

/**
 * In memory cache implementation with TTL support.
 * Items are expired and popped during mutating operations instead of using periodic checks.
 * @package M360
 */
class M360Cache
{

	private $ttl = 300;
	private $cache;

	public function __construct ($ttl = null) {
		if (!empty($ttl)) {
			$this->ttl = $ttl;
		}
		$this->cache = [];
	}

	/**
	 * Cycles through all cached items and pops the ones that are expired
	 */
	public function check_ttls () {
		$now = time();
		foreach ($this->cache as $key => $val) {
			if ($val->exp < $now) {
				//remove key
				$this->delete($key);
			}
		}
	}

	/**
	 * Set a key/value pair in the cache with a specific ttl in seconds (or the default one)
	 * @param $key
	 * @param $value
	 * @param $ttl
	 */
	public function set ($key, $value, $ttl = null) {
		$this->check_ttls();
		if (empty($ttl)) {
			$ttl = $this->ttl;
		}

		$this->cache[$key] = (object)[
			'exp' => time() + $ttl,
			'value' => $value,
		];
	}

	/**
	 * Get a cached value using its key
	 * @param $key
	 */
	public function get ($key) {
		$this->check_ttls();
		return (!empty($this->cache[$key])) ? $this->cache[$key]->value : null;
	}

	/**
	 * Remove an item from the cache using its key
	 * @param $key
	 */
	public function delete ($key) {
		unset($this->cache[$key]);
	}

	/**
	 * Clears the cache i.e. removes all saved items
	 */
	public function flush () {
		$this->cache = [];
	}
}