<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360;

use Zalora\Punyan\Logger as PunyanLogger;
use Zalora\Punyan\ZLog;

/**
 * Builds the Logger configuration and creates a new custom logger instance that attaches to the Middleware SDK
 * @type {{init(*=): void, get(): *, config: {}}}
 */
class Logger
{
	public function __construct ($config) {
		$this->defaultConfig = array(
			"logger" => array(
				"mute" => false,
				"filters" => array(
					"priority" => array(
						"priority" => "debug", //level original npm
						"operator" => ">=",
					)
				),
				"writers" => array(
					"stream" => array(
						"mute" => false,
						"url" => "php://stdout",
						"filters" => array(
							"priority" => array(
								"priority" => "warn",
								"operator" => ">=",
							),
							"ns" => array(
								"namespace" => "service",
								"searchMethod" => "contains"
							)
						),
					),
				),
			),
		);

		//npm src not needed
		//npm level = priority
		//npm stream count and period not needed
		//npm format not needed as found
		//npm streams = writers

		if ($config == "" || !property_exists($config, "logger")) {
			$this->config = $this->defaultConfig;
		} else {
			$mapping = (array)$config->logger;
			unset($mapping["src"]);
			unset($mapping["format"]);
			$mapping["filters"] = array(
				"priority" => array(
					"priority" => $mapping["level"]
				)
			);
			$mapping["writers"] = $mapping["streams"];
			unset($mapping["streams"]);
			unset($mapping["level"]);

			if (!array_key_exists("mute", $mapping)) {
				$mapping["mute"] = false;
			}

			foreach ($mapping["writers"] as $key => $streams) {
				$streams = (array)$streams;
				unset($streams["count"]);
				unset($streams["period"]);
				$streams["mute"] = false;
				if (array_key_exists("stream", $streams)) {
					$streams["url"] = $streams["stream"];
					if ($streams["url"] == "process.stdout") {
						$streams["url"] = "php://stdout";
						unset($streams["stream"]);
					}
				}
				if (array_key_exists("path", $streams)) {
					$streams["url"] = "file://" . $streams["path"];
					unset($streams["path"]);
				}
				$streams["filters"] = array(
					"priority" => array(
						"priority" => $streams["level"],
						"operator" => ">="
					)
				);
				unset($streams["level"]);
				$mapping["writers"][$key] = array("stream" => $streams);
			}
			$this->config = array("logger" => $mapping);
		}


		ZLog::setInstance(new PunyanLogger('m360Corsair', $this->config));
	}

	public function get () { //TO VALIDATE
		return new PunyanLogger('m360Corsair', $this->config);
	}
}
