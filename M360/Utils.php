<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360;

class Utils
{

	public function __construct () {

	}

	/**
	 * This method attempts to return the value of the path given in the context object
	 * @param {Object} $context
	 * @param {String} $keyName
	 * @returns {boolean|*}
	 */
	public function checkIfKeyIsInObjectAndReturnValue ($context, $keyName) {
		if (empty($keyName)) {
			return null;
		}

		if (!str_contains($keyName, ".")) {
			if(is_array($context)){
				return $context[$keyName];
			}
			else
				return $context->$keyName;
		}

		$chunks = explode(".", $keyName);

		$l1 = array_reduce($chunks, function ($acc, $v) {

			if(!isset($acc) || empty($acc)){
				$acc = array();
			}

			if (str_starts_with($v, "[")) {
				$arrPart = explode("[", $v);
				$arrPart = $arrPart[1];
				$v = explode("]", $arrPart);
				$v = $v[0];
			}

			if (str_ends_with($v, "]") && !str_starts_with($v, "[")) {
				$t = explode("[", $v);
				$leftPart = $t[0];

				$rightPart = explode("]", $t[1]);
				$firstIndex = $rightPart[0];

				array_push($acc, [$leftPart, $firstIndex]);
				return $acc;
			}

			array_push($acc, $v);
			return $acc;
		});

		return array_reduce($l1, function ($acc, $v) {
			if(is_array($v)){
				 $v = $v[0];
			}

			if(is_array($acc)){
				$acc = (isset($acc[$v])) ? $acc[$v] : null;
			}
			else if(is_object($acc)){
				$acc = (isset($acc->$v)) ? $acc->$v : null;
			}
			return $acc;
		}, $context);
	}

	/**
	 * This method build N-levels in the given object based on the path provided by splitting the path on the dot symbol
	 * The last level in the path is then assigned the path value and the function stops.
	 * @param {Object} $object
	 * @param {String} $path
	 * @param {*} $pathValue
	 */
	public function buildObjectFromPath ($object, $path, $pathValue) {
		$fieldNamePath = explode(".", $path);
		return $this->buildOneLevelDeeper($object, $fieldNamePath, 0, 'build', $pathValue);
	}

	/**
	 * This method removes the sub object in the provided object based on the given path
	 * @param {Object} $object
	 * @param {String} $path
	 */
	public function removeObjectFromPath (&$object, $path) {
		// "config.M360.common.pet": "cersei" break it 3al space

		$fieldNamePath = explode(".", $path);
		return $this->buildOneLevelDeeper($object, $fieldNamePath, 0, 'delete');
	}

	private function buildOneLevelDeeper (&$object, $levels, $counter, $operation, $pathValue = null) {

		if(is_object($object)){
			if (!empty($object->{$levels[$counter]})) {
				$object->{$levels[$counter]} = [];
			}

			$subObject = $object->{$levels[$counter]};
		}
		else{
			if (!empty($object[$levels[$counter]])) {
				$object[$levels[$counter]] = [];
			}
			$subObject = $object[$levels[$counter]];
		}


		$counter++;
		if ($counter === sizeof($levels)) {
			$counter--;
			if ($operation === 'delete') {
				if(is_object($object)){
					unset($object->$levels[$counter]);
				}
				else
					unset($object[$levels[$counter]]);
			} else {
				$object[$levels[$counter]] = $pathValue;
			}
			return $object;
		} else {
			if(!isset($subObject[$levels[$counter]])){
				$subObject[$levels[$counter]] = [];
			}
			return $this->buildOneLevelDeeper($subObject, $levels, $counter, $operation, $pathValue);
		}
	}
}

