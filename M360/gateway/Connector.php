<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360\gateway;

use M360\timeConvert;
use Curl;


class Connector
{
	private $curl;
	private $time = 0;
	public $GATEWAY_MAINTENANCE_IP = '127.0.0.1';
	public $GATEWAY_MAINTENANCE_PORT = 5000;
	public $GATEWAY_MAINTENANCE_SSL = false;
	public $serviceName;
	protected $serviceVersion;

	/**
	 * Will initiate the connector and will need crypter.
	 * @param type $serviceName
	 * @param type $serviceVersion
	 */
	public function __construct ($serviceName, $serviceVersion, $gatewayMaintenanceIP = null, $gatewayMaintenancePort = null, $gatewayMaintenanceSSL = null, Curl\Curl $curl = null) {

		if (!empty($gatewayMaintenanceIP)) {
			$this->GATEWAY_MAINTENANCE_IP = $gatewayMaintenanceIP;
		} else {
			$this->GATEWAY_MAINTENANCE_IP = empty($_SERVER['GATEWAY_MAINTENANCE_IP']) ? $this->GATEWAY_MAINTENANCE_IP : $_SERVER['GATEWAY_MAINTENANCE_IP'];
		}

		if (!empty($gatewayMaintenancePort)) {
			$this->GATEWAY_MAINTENANCE_PORT = $gatewayMaintenancePort;
		} else {
			$this->GATEWAY_MAINTENANCE_PORT = empty($_SERVER['GATEWAY_MAINTENANCE_PORT']) ? $this->GATEWAY_MAINTENANCE_PORT : $_SERVER['GATEWAY_MAINTENANCE_PORT'];
		}

        if (!empty($gatewayMaintenanceSSL)) {
			$this->GATEWAY_MAINTENANCE_SSL = $gatewayMaintenanceSSL;
		} else {
			$this->GATEWAY_MAINTENANCE_SSL = empty($_SERVER['GATEWAY_MAINTENANCE_SSL']) ? $this->GATEWAY_MAINTENANCE_SSL : $_SERVER['GATEWAY_MAINTENANCE_SSL'];
		}
		$this->serviceName = $serviceName;
		$this->serviceVersion = $serviceVersion;

		if (!empty($curl)) {
			$this->curl = $curl;
		} else {
			$this->curl = new Curl\Curl();
		}
	}

	/**
	 * Invoke will call make request to create a curl call.
	 * Post and get are available
	 * @param type $context
	 * @return type
	 */
	public function invoke ($context) {
		$ip = !isset($context['ip']) || empty($context["ip"]) ? $this->GATEWAY_MAINTENANCE_IP : $context['ip'];
		$port = !isset($context['port']) || empty($context["port"]) ? $this->GATEWAY_MAINTENANCE_PORT : $context['port'];
		$protocol = 'http';
        if(isset($context['ssl'])){
            $protocol = 'https';
        }
        else if($this->GATEWAY_MAINTENANCE_SSL === 'true'){
            $protocol = 'https';
        }

		$proxyOptions = array(
			"uri" => "{$protocol}://{$ip}:{$port}" . $context['route'],
			"json" => true,
			"headers" => array()
		);
		if (isset($context["headers"])) {
			$proxyOptions["headers"] = $context["headers"];
		}

		if (isset($this->serviceName)) {
			$proxyOptions["headers"]["service"] = $this->serviceName;
			$proxyOptions["headers"]["version"] = $this->serviceVersion;
		}

		$proxyOptions["qs"] = array();
		if (isset($context["qs"])) {
			$proxyOptions["qs"] = $context["qs"];
		}
		if (isset($context["body"])) {
			$proxyOptions["body"] = $context["body"];
		}
		$method = "get";
		if (isset($context["method"])) {
			$method = $context["method"];
		}
		$counter = 1;
		$max = isset($context["retryOnFailure"]) ? $context["retryOnFailure"] : 1;
		$delay = isset($context["retryDelay"]) ? $context["retryDelay"] : 5000;

		return $this->_makeRequest($counter, $max, $method, $proxyOptions, $delay, $context);
	}

	/**
	 * Calls the curl method accordingly to post and get and will return an exception or a body response.
	 *
	 * It has a "circuit breaker" in case the call is not reachable and in case the connection gets lost
	 * @param int $counter
	 * @param type $max
	 * @param type $method
	 * @param array $proxyOptions
	 * @param type $delay
	 * @param type $context
	 * @return type
	 * @throws \Exception
	 */
	private function _makeRequest ($counter, $max, $method, $proxyOptions, $delay, $context) {
		$inTime = new timeConvert\Time($delay);
		$preetyTime = $inTime->milToTime();
		$curl = $this->curl;
		$curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
		$curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);
		$curl->setOpt(CURLOPT_AUTOREFERER, true);
		$proxyOptions = (object)$proxyOptions;
		if (property_exists($proxyOptions, "headers")) {
			foreach ($proxyOptions->headers as $key => $value) {
				$curl->setHeader($key, $value);
			}
		}
		$url = $proxyOptions->uri;
		if ($method == "post") {
			$curl->setHeader("Content-Type", "application/json");
			$url .= !empty($proxyOptions->qs) ? '?' . http_build_query($proxyOptions->qs) : "";
			$curl->post($url, json_encode($proxyOptions->body));
		} elseif ($method == "get") {
			$curl->get($url, $proxyOptions->qs);
		}

		$body = json_decode($curl->getResponse(), false);
		if ($curl->curl_error || !isset($body) || empty($body)) {
			if ($counter < $max) {
				$counter++;
				echo("\nRequest Failed. Retrying { $counter }/{ $max } in { $preetyTime } hours ...");
				sleep($inTime->milToSecs());
				$this->_makeRequest($counter, $max, $method, $proxyOptions, $delay, $context);
			} else {
				echo("\nRequest Failed after { $counter }/{ $max } attempts!");
				throw new \Exception($curl->error_message, $curl->error_code);
			}
		} elseif (property_exists($body, "result")) {
			if ($body->result == false) {
				$errors = [];
				if (property_exists($body, 'error')) {
					array_push($errors, $body->message);
				}
				if (property_exists($body, 'errors')) {
					foreach ($body->errors as $key => $codes) {
						array_push($errors, "Error {$codes->code} : {$codes->message}\n");
					}
				}
				echo("\nFailed getting service after { $counter }/{ $max } attempts!");
				if ($counter < $max) {
					$counter++;
					echo("\nRequest Failed. Retrying { $counter }/{ $max } in { $preetyTime } hours ...");
					sleep($inTime->milToSecs());
					$this->_makeRequest($counter, $max, $method, $proxyOptions, $delay, $context);
				}
				echo("\nRequest Failed after { $counter }/{ $max } attempts!");
				throw new \Exception(implode(",", $errors));

			} else {
				if (gettype($body->data) == 'string') {
					$body->data = json_decode($body->data, false);
				}
				$curl->reset();
				return $body->data;
			}
		} else {
			$curl->reset();
			return $body;
		}
	}
}
