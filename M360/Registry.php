<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360;

class Registry
{
	public $mycache;
	public $GatewayConnector;

	/**
	 * Creates registry init
	 */
	public function __construct (M360Cache $cache = null) {
		if (!empty($cache)) {
			$this->mycache = $cache;
		} else {
			$this->mycache = new M360Cache();
		}
	}

	/**
	 * Will receive the gateway connector object
	 * @param type $gatewayConnector
	 */
	public function setGatewayConnector ($gatewayConnector) {
		$this->GatewayConnector = $gatewayConnector;
	}

	/**
	 * return gateway connector
	 */
	public function getGatewayConnector () {
		return $this->GatewayConnector;
	}

	/**
	 * This method will get custom registry or resources
	 * @param name registry|custom|resources
	 * @param section (optional)will get the registry's section
	 */
	public function get ($name, $section = "") {
		$registry = $this->mycache->get($name);
		if (empty($registry)) {
			if ($this->load()) {
				$registry = $this->mycache->get($name);
				return $this->_getRegSection($registry, $section);
			}
		} else {
			return $this->_getRegSection($registry, $section);
		}
	}

	/**
	 * pull registry based on name and section
	 * @param type $registry
	 * @param {String} $section
	 * @return boolean
	 */
	protected function _getRegSection ($registry, $section) {
		if (empty($registry)) {
			return false;
		}
		if (!empty($section) && gettype($section) == "string" && property_exists($registry, trim($section))) {
			return $registry->$section;
		} else {
			return $registry;
		}
	}

	/**
	 * Will call gateway invoke and validate the data from the registry and will cache it
	 */
	public function load () {
		$context = array(
			"method" => 'get',
			"route" => '/registry/',
			"decrypt" => true,
			"retryOnFailure" => 3,
			"retryDelay" => isset($_SERVER["MW_TESTING"]) ? 1 : 5000,
		);

		$data = $this->GatewayConnector->invoke($context);
		if ($data && $data->registry) {
			$TTL = $data->registry->_TTL / 1000;
			$this->mycache->set('registry', $data->registry, $TTL);
			$this->mycache->set('custom', $data->custom, $TTL);
			$this->mycache->set('resources', $data->resources, $TTL);
			$this->mycache->set('databases', $data->databases, $TTL);
		}

		return true;
	}

	/**
	 * will reload registry
	 * @return {JSON}
	 */
	public function reload () {
		$this->load();
		return $this->get("registry", "");

	}

	/**
	 * will clear cache for registry, custom, resources and databases
	 */
	public function clear () {
		$this->mycache->delete('registry');
		$this->mycache->delete('custom');
		$this->mycache->delete('resources');
		$this->mycache->delete('databases');

		return true;
	}

}

class CustomRegistry extends Registry
{
	/**
	 * Will get custom registry and return it from memcache
	 * @param type $name
	 * @param type $section
	 * @return type
	 */
	public function get ($name, $section = "") {
		return $this->_getRegSection($this->mycache->get("custom"), $section);
	}
}

class Resource extends Registry
{
	/**
	 * will get resource from the name and section
	 * @param type $name
	 * @param type $section
	 * @return type
	 */
	public function get ($name, $section = "") {
		return $this->_getRegSection($this->mycache->get("resources"), $section);
	}
}

class Databases extends Registry
{
	/**
	 * This method returns the requested database based on its name and type.
	 * if the databse is multitenant, the tenantCode parameter is required to populate the db name.
	 * @param type $type
	 * @param type $name
	 * @param type $tenantCode
	 * @return type
	 */

	public function getDB ($type, $name, $tenantCode = "") {
		$tenantCode = trim($tenantCode);
		if ($type == "multitenant" && $tenantCode == "") {
			return null;
		}

		return $this->_getDB($type, $name, $tenantCode);

	}

	private function _getDB ($type, $name, $tenantCode) {
		$databases = (array)$this->mycache->get("databases");
		if (count($databases) > 0 && array_key_exists($type, $databases)) {
			if (property_exists($databases[$type], $name)) {
				$databases[$type] = (array)$databases[$type];
				$db = (array)$databases[$type][$name];
				$clusterName = $db["cluster"];
				$db["cluster"] = array();
				$db["cluster"] = parent::get("resources", $clusterName);
				$db["cluster"]->name = $clusterName;
				$db["name"] = $this->_fixDBName($type, $name, $db["name"], $tenantCode);
				return $db;
			} else {
				foreach ($databases[$type] as $dbName => $dbObject) {
					$databases[$type] = (array)$databases[$type];
					$clusterName = $databases[$type][$dbName]->cluster;
					$databases[$type][$dbName]->cluster = array();
					$databases[$type][$dbName]->cluster = parent::get("resources", $clusterName);
					$databases[$type][$dbName]->cluster->name = $clusterName;
					$databases[$type][$dbName]->name = $this->_fixDBName($type, $name, $databases[$type][$dbName]->name, $tenantCode);
				}
				return $databases[$type];
			}
		} else {
			return NULL;
		}
	}

	/**
	 * will clear the name in case it's repeated
	 * @param type $type
	 * @param type $main
	 * @param type $prefixed
	 * @param type $tenantCode
	 * @return type
	 */
	private function _fixDBName ($type, $main, $prefixed, $tenantCode) {
		if ($type == "multitenant") {
			$prefix = str_replace($main, "", $prefixed);
			if ($prefix == $prefixed) {
				if (!empty($main)) {
					return $tenantCode . "_" . $prefix;
				}
				return $tenantCode . "_" . $main;
			}
			return $tenantCode . "_" . $main;
		}
		return $prefixed;
	}
}

