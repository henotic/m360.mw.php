<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360;

class Schema
{
	private $oneAPI;
	private $serviceContract;
	private $mwConfig;

	/**
	 * Method that creates a validation schema for the middleware configuration and contract
	 */
	public function __construct () {
		$this->oneAPI = '{
            "patternProperties": {
                "patternReplace": {
                    "type": "object",
                    "required": ["access", "label"],
                    "properties": {
                        "access": {"type": "boolean"},
                        "label": {"type": "string"},
                        "rbac": {
                            "type": "object",
                            "additionalProperties": false,
                            "properties": {
                                "fields": {
                                    "oneOf": [
                                        {
                                            "type": "boolean"
                                        },
                                        {
                                            "type": "array",
                                            "items": {
                                                "type": "string"
                                            }
                                        }
                                    ]
                                },
                                "resources": { "type": "boolean" },
                                "conditions": { "type": "boolean" }
                            }
                        }
                    },
                    "additionalProperties": false
                }
            },
            "additionalProperties": false
        }';

		$pattern = "^[\\/]([\\:?\\p{L}\\p{N}\\p{Pd}_\\-\\/?])*";
		$this->oneAPI = json_decode($this->oneAPI, true);
		$this->oneAPI["patternProperties"][$pattern] = $this->oneAPI["patternProperties"]["patternReplace"];
		unset($this->oneAPI["patternProperties"]["patternReplace"]);
		$this->oneAPI = json_encode($this->oneAPI);


		$this->serviceContract = '{
            "properties": {
                "swagger": {
					"type": "string",
					"format": "uri"
				},
				"stats": {
					"type": "string",
					"format": "uri"
				},
                "name": {
                    "type": "string",
                    "minLength": 4,
                    "pattern": "^[a-z][0-9a-z]{3,}$"
                },
                "group": {
                    "type": "string",
                    "minLength": 4
                },
                "version": {
                    "type": "number",
                    "minimum": 1
                },
                "weight": {
                    "type": "number",
                    "minimum": 1
                },
                "ports": {
                    "type": "object",
                    "properties": {
                        "data": {
                            "type": "number",
                            "minimum": 1,
                            "maximum": 65535
                        },
                        "maintenance": {
                            "type": "number",
                            "minimum": 1,
                            "maximum": 65535
                        }
                    },
                    "required": ["data"],
                    "additionalProperties": false
                },
                "apis": {
	                "type": "object",
	                "properties": {
	                    "main": {
	                        "type": "object",
	                        "properties": {
	                            "get": ' . $this->oneAPI . ',
	                            "post": ' . $this->oneAPI . ',
	                            "put": ' . $this->oneAPI . ',
	                            "patch": ' . $this->oneAPI . ',
	                            "delete": ' . $this->oneAPI . '
	                        },
	                        "additionalProperties": false
	                    },
	                    "maintenance": {
	                        "type": "object",
	                        "properties": {
	                            "get": ' . $this->oneAPI . ',
	                            "post": ' . $this->oneAPI . ',
	                            "put": ' . $this->oneAPI . ',
	                            "patch": ' . $this->oneAPI . ',
	                            "delete": ' . $this->oneAPI . '
	                        },
	                        "additionalProperties": false
	                    }
	                },
	                "required": ["main"],
	                "additionalProperties": false
	            },
	            "host": {
					"type": "object",
					"properties": {
                        "ssl": {
                            "type": "boolean"
                        },
						"weight": {
							"type": "number",
							"minimum": 1
						}
					},
					"required": [ "ssl", "weight" ],
					"additionalProperties": false
				}
            },
            "required": ["name", "group", "version", "ports", "apis"],
            "additionalProperties": false
        }';

		$this->mwConfig = '{
            
            "properties": {
                "ip": {"type": "string"},
                "platform": {
					"type": "string",
					"enum": [ "docker", "kubernetes", "manual", "other" ]
				},
                "type": {"type": "string", "enum": ["laravel", "symfony", "job"]},
                "contract": ' . $this->serviceContract . '
            },
            "required": ["contract", "ip", "platform", "type"]
        }';
	}

	/**
	 * Method that returns the middleware config in a json format
	 * @param {String} $encoded
	 * @return {JSON}
	 */
	public function getMWConfig ($encoded) {
		return $encoded ? json_decode($this->mwConfig, false) : $this->mwConfig;
	}
}
