<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360;

use M360\gateway;

class User
{

	private $GatewayConnector;

	public function __construct () {

	}

	/**
	 * Will receive the gateway connector object
	 * @param type $gatewayConnector
	 */
	public function setGatewayConnector ($gatewayConnector) {
		$this->GatewayConnector = $gatewayConnector;
	}

	/**
	 * This methods checks that m360 is provided in the request headers.
	 * Then it scans to find the user information inside m360 and returns it.
	 * @param {Object} $request
	 */
	public function get ($request) {
		if (!empty($request) && array_key_exists("m360", $request) && !empty($request["m360"])) {
			$m360 = json_decode($request["m360"], false);
			return property_exists($m360, "user") ? $m360->user : null;
		}
		return null;
	}

	/**
	 * This method finds the user(s) information and configuration using the gateway.
	 * It invokes the maintenance APIs of the gateway to fetch and decrypt this information.
	 * @param {Object} $context
	 */
	public function find ($context) {
		$id = array_key_exists("id", $context) ? $context["id"] : null;
		$start = array_key_exists("start", $context) ? $context["start"] : 1;
		$limit = array_key_exists("limit", $context) ? $context["limit"] : 100;
		$group = array_key_exists("group", $context) ? $context["group"] : null;
		$service = array_key_exists("service", $context) ? $context["service"] : null;
		$version = array_key_exists("version", $context) ? $context["version"] : null;
		$tenant = array_key_exists("tenant", $context) ? $context["tenant"] : null;

		$url = "/users";
		$url .= ($id && $id !== '') ? "/" . $id : '';
		$qs = array();

		if ($id != null) {
			$qs["start"] = $start;
			$qs["limit"] = $limit;
		}

		if ($group) {
			$qs["group"] = $group;
		}

		if ($tenant != null) {
			$qs["tenant"] = $tenant;
		}

		if ($service != null) {
			$qs["microservice"] = $service;
		}

		if ($version != null) {
			$qs["version"] = $version;
		}

		return $this->GatewayConnector->invoke(array(
			"decrypt" => true,
			"route" => $url,
			"qs" => $qs
		));
	}
}

