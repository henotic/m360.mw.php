<?php
/*
 *  Copyright (C) 2021 Corsair M360, Inc - All Rights Reserved.
 *  Unauthorized copying of this file, via any medium is strictly prohibited.
 *  Proprietary and confidential.
 */

namespace M360\timeConvert;
class Time{
    public $milliseconds;
    /**
     * Will receive millisecods as integer
     * @param int $milliseconds
     */
    public function __construct($milliseconds){
        $this->milliseconds = $milliseconds;
    }
    /**
     * will prettify the milliseconds to time
     * like HH:MM:SS.mm
     * @return type
     */
    public function milToTime(){
        $seconds = floor($this->milliseconds / 1000);
        $minutes = floor($seconds / 60);
        $hours = floor($minutes / 60);
        $milliseconds = $this->milliseconds % 1000;
        $seconds = $seconds % 60;
        $minutes = $minutes % 60;
    
        $format = '%u:%02u:%02u.%03u';
        $time = sprintf($format, $hours, $minutes, $seconds, $milliseconds);
        return rtrim($time, '0');
    }
    /**
     * Will convert milliseconds to seconds
     * @return type
     */
    public function milToSecs(){
        return floor($this->milliseconds / 1000);
    }
}